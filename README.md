# Unstoppable

Unstoppable decentralized funds raising platform

## Setup
```bash
npm i
npm run bootstrap
npm run build
```

## Use cases
Use cases diagramm are in the file [usecases.drawio](./docs/usecases.drawio)  
Please use Draw.io editor to view and work with.