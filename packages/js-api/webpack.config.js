const path = require('path');

module.exports = {
    mode: 'production',
    entry: './src',
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'index.js',
        library: 'UnstoppableJsAPI',
        libraryTarget: 'umd'
    }
};
