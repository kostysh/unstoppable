import chai from 'chai';
import { serializeMilestone } from '../../../src/utils';
import { utils as web3utils } from 'web3';
chai.should();

describe('Projects utils', () => {

    describe('#serializeMilestone', () => {

        it('should serialize milestone object', async () => {
            const milestone = {
                duration: web3utils.toBN('100'),
                softCap: web3utils.toBN('60000000000000000000'),
                hardCap: web3utils.toBN('100000000000000000000000000')
            };
            console.log(serializeMilestone(milestone));
            //(serializeMilestone()).should.be.true;
        });
    });
});
