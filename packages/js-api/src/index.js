/**
 * Unstoppable Js API
 * API and utility functions to the Unstoppable decentralized funds raising platform
 * 
 * @file index.js
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 * @date 2019
 */

import * as utils from './utils';


/** UnstoppableJsAPI class */
export default class UnstoppableJsAPI {

    static get utils() {
        return utils;
    }

    /**
     * Creates an instance of UnstoppableJsAPI
     * @param {Object} options
     * @memberof UnstoppableJsAPI
     */
    constructor(options = {}) {

    }
}
