/**
 * Ensuring expected parameters helper
 * 
 * @file expect.js
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 * @date 2019
 */
import { utils as web3utils } from 'web3';

export const OPTIONS_REQUIRED = 'Options are required';
export const MODEL_REQUIRED = 'Model is required';
export const MODEL_TYPE_OPTIONS_REQUIRED = 'Model type options required';
export const WRONG_TYPE = 'Wrong type of the property';
export const ADDRESS_REQUIRED = 'Address required';
export const BN_REQUIRED = 'BN instance required';

/**
 * ExpectError class
 *
 * @class ExpectError
 * @extends {Error}
 */
export class ExpectError extends Error {

    /**
     * Creates an instance of ExpectError.
     * @param {String} message
     * @memberof ExpectError
     */
    constructor(message = 'Unknown error', ...args) {
        super(message);
        this.args = args;
    }
}

/**
 * Ensuring expected parameters helper
 *
 * @param {Object} options
 * @param {Object} model
 */
export const all = (options = {}, model = {}) => {

    if (typeof options !== 'object' || Object.keys(options).length === 0) {

        throw new ExpectError(OPTIONS_REQUIRED);
    }

    if (typeof model !== 'object' || Object.keys(model).length === 0) {

        throw new ExpectError(MODEL_REQUIRED);
    }

    for (let key of Object.keys(model)) {

        if (!model[key].type) {

            throw new ExpectError('Model property must have a "type" defined');
        }

        let value = key.split('.').reduce((acc, part) => {
            return acc && acc[part] !== undefined ? acc[part] : null;
        }, options);

        switch (model[key].type) {
            case 'enum':

                if (!model[key].values || !Array.isArray(model[key].values)) {

                    throw new ExpectError(MODEL_TYPE_OPTIONS_REQUIRED, {
                        expected: 'enum',
                        values: model[key].values,
                        key,
                        value
                    });
                }

                if (!model[key].values.includes(value)) {

                    throw new ExpectError(WRONG_TYPE, {
                        expected: 'enum',
                        values: model[key].values,
                        key,
                        value
                    });
                }

                break;

            case 'address':

                if (!new RegExp('^0x[a-fA-F0-9]{40}$').test(value)) {

                    throw new ExpectError(ADDRESS_REQUIRED, {
                        expected: 'address',
                        key,
                        value
                    });
                }

                break;

            case 'hash':

                if (!new RegExp('^0x[a-fA-F0-9]{64}$').test(value)) {

                    throw new ExpectError(ADDRESS_REQUIRED, {
                        expected: 'hash',
                        key,
                        value
                    });
                }

                break;

            case 'bn':

                if (!web3utils.isBN(value)) {

                    throw new ExpectError(BN_REQUIRED, {
                        expected: 'bn',
                        key,
                        value
                    });
                }

                break;
            
            case 'functionOrMember':
                
                if (typeof value === 'function') {

                    // It is OK
                    break;
                }
                // If not then follow the next rule
                
            // eslint-disable-next-line no-fallthrough
            case 'member':
                
                if (!model[key].provider || typeof model[key].provider !== 'object') {

                    throw new ExpectError(`Provider object must be defined as "provider" model option for "${key}"`);
                }

                if (typeof value !== 'string') {

                    throw new ExpectError(`Property with "member" type must be a string but actually, it is a "${typeof value}"`);
                }

                const memberValue = value.split('.').reduce((acc, part) => {
                    return acc && acc[part] !== undefined ? acc[part] : null;
                }, model[key].provider);

                if (!memberValue) {

                    throw new ExpectError('Not a member', {
                        expected: model[key].type,
                        provider: model[key].provider,
                        key,
                        value
                    });
                }

                break;

            default:
                
                if (typeof value !== model[key].type && 
                    (model[key].required === true || model[key].required === undefined)) {

                    throw new ExpectError(WRONG_TYPE, {
                        expected: model[key].type,
                        key,
                        value
                    });
                }
        }
    }
};

