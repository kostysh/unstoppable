import * as expect from './expect.js';
import { utils as web3utils } from 'web3';

export const serializeMilestone = (milestone = {}) => {
    
    expect.all(milestone, {
        'duration': {
            type: 'bn'
        },
        'softCap': {
            type: 'bn'
        }
        ,
        'hardCap': {
            type: 'bn'
        }
    });

    return ['duration', 'softCap', 'hardCap']
        .map(key => web3utils.numberToHex(milestone[key]))
        .reduce((acc, val, index) => {

            if (index === 1) {
                acc = web3utils.padLeft(acc, 32).slice(2);
            }

            return `${acc}${web3utils.padLeft(val, 32).slice(2)}`;
        });
};
