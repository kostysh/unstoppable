const AssetsLib = artifacts.require('AssetsLib');
const ProjectsLib = artifacts.require('ProjectsLib');
const AssetsRegistry = artifacts.require('AssetsRegistry');
const ProjectsRegistry = artifacts.require('ProjectsRegistry');
const ProjectsRegistryTests = artifacts.require('ProjectsRegistryTests');

module.exports = async (deployer, network /*,  accounts */) => {
    await deployer.deploy(AssetsLib);
    await deployer.deploy(ProjectsLib);
    deployer.link(AssetsLib, AssetsRegistry);
    deployer.link(ProjectsLib, ProjectsRegistry);

    if (network === 'ganache' || network === 'coverage') {
        deployer.link(ProjectsLib, ProjectsRegistryTests);
    }
};
