pragma solidity 0.5.11;

import "openzeppelin-solidity/contracts/introspection/ERC165Checker.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/utils/ReentrancyGuard.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "./registries/IProjectsRegistry.sol";


contract Merchant is Ownable, ReentrancyGuard {

    using SafeMath for uint256;

    bytes4 private constant _INTERFACE_ID_PROJECTS_REGISTRY = 0xffaef45d;

    /// @dev Link to ProjectsRegistry instance
    IProjectsRegistry private projectsRegistry;

    /// @dev Obtained payments
    mapping (address => uint256) private balances;

    /**
     * @dev This event will be emitted when someone sends ether to the contract
     * @param from Sender address
     * @param value Ether value
     */
    event PaymentReceived (
        address from,
        uint256 value
    );

    /**
     * @dev This event will be emitted when sender did withdrawal of funds
     * @param sender Sender address
     * @param value Ether value
     */
    event Withdrawal (
        address sender,
        uint256 value
    );

    /**
     * @dev This event will be emitted when investment has been made
     * @param projectId Project Id
     * @param from Address of investor
     * @param value Ether value
     */
    event InvestmentMade (
        bytes32 projectId,
        address from,
        uint256 value
    );

    /**
     * @dev Merchant constructor
     * @param _projectsRegistry Unstoppable contract instance address
     */
    constructor(address _projectsRegistry) public {
        require(
            ERC165Checker._supportsInterface(_projectsRegistry, _INTERFACE_ID_PROJECTS_REGISTRY), 
            "NOT_SUPPORTED_PROJECTS_REGISTRY"
        );
        projectsRegistry = IProjectsRegistry(_projectsRegistry);
    }

    /**
     * @dev Fallback function
     */
    function() external payable {
        
        if (msg.value > 0) {            
            
            balances[msg.sender] = balances[msg.sender].add(msg.value);
            emit PaymentReceived(msg.sender, msg.value);
        }
    }

    /**
     * @dev Withdraw funds
     * @param value Amount to withdraw
     */
    function withdraw(uint256 value) external nonReentrant {
        require(balances[msg.sender] >= value, "INSUFICIENT_FUNDS");
        balances[msg.sender] = balances[msg.sender].sub(value);
        msg.sender.transfer(value);
        emit Withdrawal(msg.sender, value);
    }

    /**
     * @dev Get balance for the owner address
     * @param investor Balance of the investor
     * @return uint256 Balance value
     */
    function balanceOf(address investor) external view returns(uint256) {
        return balances[investor];
    }

    /**
     * @dev Make investment from the name of "from" address. 
     * Can be called by the whaitelisted address only
     * @param projectId Project Id
     * @param from Investor address
     * @param value Investment value
     */
    function makeInvestment(
        bytes32 projectId,
        address from,
        uint256 value
    ) external onlyOwner nonReentrant {
        require(from != address(0), "WRONG_FROM_ADDRESS");
        require(projectsRegistry.isProject(projectId), "PROJECT_NOT_FOUND");
        require(balances[from] >= value, "INSUFICIENT_FUNDS");

        balances[from] = balances[from].sub(value);
        (bool success, ) = address(projectsRegistry).call.value(value)(abi.encodeWithSignature(
            "investFrom(bytes32,address,uint256)", 
            projectId, 
            from, 
            value
        ));
        require(success, "UNABLE_TO_PROCESS_PAYMENT");
        emit InvestmentMade(projectId, from, value);
    }
}
