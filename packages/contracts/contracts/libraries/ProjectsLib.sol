pragma solidity 0.5.11; 

import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "../IUnstoppable.sol";
import "./UidsLib.sol";
import "./Voting.sol";


/**
 * @title Projects Library
 * @dev This library holds projects data structures and management logic
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 */
library ProjectsLib {

    using SafeMath for uint256;

    /**
     * @dev List of acceptable url types
     */
    enum UrlType {
        Ipfs,
        Swarm,
        Arweave
    }

    /**
     * @dev List of allowed project states
     */
    enum State {
        Uninitialized,
        Created,
        Closed,
        Campaign,
        CampaignFailed,
        Started,
        Voting,
        VotingFailed,
        Finished
    }

    /**
     * @dev Project Milestone structure
     * @param id Unique milestone Id
     * @param duration Milestone duration in days
     * @param budget Milestone budget
     * @param started Time the milestone has been started
     * @param votes Votes balance
     */
    struct Milestone {
        bytes32 id;
        uint256 duration;
        uint256 budget;
        uint256 started;
        uint256 votes;
    } 
    
    /**
     * @dev Project structure
     * @param owner Project owner address
     * @param id Unique project Id
     * @param url Link or Id of a detailed project description that available online
     * @param urlType Type of description link protocol
     * @param assetId Project asset Id
     * @param softCap Soft cap in assets units
     * @param hardCap Hard cap in assets units
     * @param startDate Start date of funds raising process (unixtime)
     * @param stopDate Stop date of funds raising process (unixtime)
     * @param milestone Current milestone
     */
    struct Project {
        address owner;
        bytes32 id;
        bytes32 url;
        UrlType urlType;
        bytes32 assetId;
        uint256 softCap;
        uint256 hardCap;
        uint256 startDate;
        uint256 stopDate;
        uint256 milestone;
    }

    /**
     * @dev Report structure
     * @param reportId Report Id
     * @param url Link or Id of a milestone report that available online
     * @param urlType Type of report link protocol
     */
    struct Report {
        bytes32 reportId;
        bytes32 url;
        UrlType urlType;
    }

    /**
     * @dev Reports structure
     * @param milestones List of reports for each milestone
     */
    struct Reports {
        mapping (uint256 => Report[]) milestones;// milestoneNumber => Report[]        
    }

    /**
     * @dev Investments structure
     * @param values List of invested values
     */
    struct Investments {
        mapping (bytes32 => uint256) values;// projectId => investmentValue
    }

    /**
     * @dev Projects storage
     * @param serviceController Root controller contract instance
     * @param transitionsTable Project states transition table config
     * @param states Projects states
     * @param registry Projects registry
     * @param milestones Projects milestones
     * @param ids Projects Ids
     * @param balances Projects balances
     * @param investors Lists of investors
     * @param investments List of investors investments
     * @param withdrawal List of project withdrawals
     * @param fees Service fees charged from projects
     */
    struct Projects {
        IUnstoppable serviceController;
        mapping (uint256 => State[]) transitionsTable;// uint256(State) => State[]
        mapping (bytes32 => State) states;// projectId => state
        mapping (bytes32 => Project) registry;// projectId => Project
        bytes32[] ids;// projectId[]
        mapping (bytes32 => Milestone[]) milestones;// projectId => Milestone[]
        mapping (bytes32 => Reports) reports;// projectId => (milestoneNumber => Report[])
        mapping (bytes32 => bytes32) reportsIds;// reportId => projectId
        mapping (bytes32 => uint256) balances;// projectId => projectBalance
        mapping (bytes32 => address[]) investors;// projectId => investorAddress[]
        mapping (address => Investments) investments;// investorAddress => (projectId => investmentValue)
        mapping (bytes32 => uint256) withdrawals;// milestoneId => withdrawnBalance
        mapping (bytes32 => uint256) fees;// projectId => serviceFee
    }

    /**
     * @dev This modifier allows function execution if provided projectId is existed
     * @param store Projects storage
     * @param projectId Project Id
     */
    modifier existedProject(
        Projects storage store,
        bytes32 projectId
    ) {
        require(isProject(store, projectId), "PROJECT_NOT_FUND");
        _;
    }

    /**
     * @dev Check is project exists
     * @param store Projects storage
     * @param projectId Project Id
     * @return bool Project existance
     */
    function isProject(
        Projects storage store,
        bytes32 projectId
    ) internal view returns (bool) {
        return store.registry[projectId].id != bytes32("");
    }

    /**
     * @dev Get project owner
     * @param store Projects storage
     * @param projectId Project Id
     * @return address Project existance
     */
    function getOwner(
        Projects storage store,
        bytes32 projectId
    ) internal view existedProject(store, projectId) returns (address) {
        return store.registry[projectId].owner;
    }

    /**
     * @dev Get project state
     * @param store Projects storage
     * @param projectId Project Id
     * @return address Project existance
     */
    function getState(
        Projects storage store,
        bytes32 projectId
    ) internal view existedProject(store, projectId) returns (State) {
        return store.states[projectId];
    }

    /**
     * @dev Get project
     * @param store Projects storage
     * @param projectId Project Id
     * @return address Project existance
     */
    function getProject(
        Projects storage store,
        bytes32 projectId
    ) internal view existedProject(store, projectId) returns (Project storage) {
        return store.registry[projectId];
    }

    /**
     * @dev Get project milestones
     * @param store Projects storage
     * @param projectId Project Id
     * @return Milestone[] List of milestones
     */
    function getMilestones(
        Projects storage store,
        bytes32 projectId
    ) internal view existedProject(store, projectId) returns (Milestone[] storage) {
        return store.milestones[projectId];
    }

    /**
     * @dev Get current project milestone number
     * @param store Projects storage
     * @param projectId Project Id
     * @return uint256 Current milestone
     */
    function getCurrentMilestone(
        Projects storage store,
        bytes32 projectId
    ) internal view existedProject(store, projectId) returns (uint256) {
        return store.registry[projectId].milestone;
    }

    /**
     * @dev Check is current milestone is final milestone
     * @param store Projects storage
     * @param projectId Project Id
     * @return bool
     */
    function isFinalMilestone(
        Projects storage store,
        bytes32 projectId
    ) internal view existedProject(store, projectId) returns (bool) {
        return store.registry[projectId].milestone == store.milestones[projectId].length.sub(1);
    }

    /**
     * @dev Check is project started
     * @param store Projects storage
     * @param projectId Project Id
     * @param time Time for check
     * @return address Project existance
     */
    function isStarted(
        Projects storage store,
        bytes32 projectId,
        uint256 time
    ) internal view existedProject(store, projectId) returns (bool) {
        return store.states[projectId] == State.Started && 
            time >= store.registry[projectId].startDate && 
            time < store.registry[projectId].stopDate;
    }

    /**
     * @dev Check is project not started
     * @param store Projects storage
     * @param projectId Project Id
     * @param time Time for check
     * @return address Project existance
     */
    function isNotStarted(
        Projects storage store,
        bytes32 projectId,
        uint256 time
    ) internal view existedProject(store, projectId) returns (bool) {
        return store.states[projectId] != State.Started && 
            (time < store.registry[projectId].startDate || 
            time >= store.registry[projectId].stopDate);
    }

    /**
     * @dev Check is project expired
     * @param store Projects storage
     * @param projectId Project Id
     * @param time Time for check
     * @return address Project existance
     */
    function isExpired(
        Projects storage store,
        bytes32 projectId,
        uint256 time
    ) internal view existedProject(store, projectId) returns (bool) {
        return time <= store.registry[projectId].stopDate;
    }

    /**
     * @dev Check is project ragequit allowed
     * @param store Projects storage
     * @param projectId Project Id
     * @param currentTime Current time
     * @return address Project existance
     */
    function isRageQuitAllowed(
        Projects storage store,
        bytes32 projectId,
        uint256 currentTime
    ) internal view existedProject(store, projectId) returns (bool) {
        // @todo Write a test for the case where milestone report not published in time
        ( , uint256 milestoneStop) = milestoneDates(store, projectId);
        return store.states[projectId] == State.Campaign || 
            (store.states[projectId] == State.Started && currentTime > milestoneStop) || 
            store.states[projectId] == State.Voting;
    }

    /**
     * @dev Calculate voting result for the current milestone
     * @param store Projects storage
     * @param projectId Project Id
     * @return uint256 value Accepted votes
     * @return uint256 threshold Votes value required to success
     */
    function getVotingResult(
        Projects storage store,
        bytes32 projectId
    ) internal view returns (
        uint256 value,
        uint256 threshold
    ) {
        value = store.milestones[projectId][store.registry[projectId].milestone].votes;
        // @todo Make threshold calculation configurable
        ( , , uint256 nativeFactor, ) = store.serviceController.getAsset(store.registry[projectId].assetId);
        (uint256 milestoneFunds, ) = calculateAllowedWithdrawal(store, projectId, store.registry[projectId].milestone.add(1));
        uint256 baseTokens = store.serviceController.calculateTokens(
            milestoneFunds, 
            store.registry[projectId].softCap, 
            nativeFactor
        );
        threshold = Voting.sqrt(baseTokens.div(2).add(1));//SQRT from 50% of baseTokens + 1
    }

    /**
     * @dev Check is transition to the new state allowed
     * @param store Projects storage
     * @param projectId Project Id
     * @param newState Future state
     */
    function isTransitionAllowed(
        Projects storage store,
        bytes32 projectId,
        State newState
    ) internal view existedProject(store, projectId) returns (bool) {
        State[] storage allowedStates = store.transitionsTable[uint256(store.states[projectId])];
        bool transitionAllowed = false;

        for (uint256 i = 0; i < allowedStates.length; i++) {
            
            if (allowedStates[i] == newState) {
                transitionAllowed = true;
            }
        }

        return transitionAllowed;
    }

    /**
     * @dev Change project state to the new one
     * @param store Projects storage
     * @param projectId Project Id
     * @param newState New project state
     * @param currentTime Current time
     */
    function moveToState(
        Projects storage store,
        bytes32 projectId,
        State newState,
        uint256 currentTime
    ) internal existedProject(store, projectId) returns (State) {
        require(isTransitionAllowed(store, projectId, newState), "TRANSITION_NOT_ALLOWED");
        State oldState = store.states[projectId];
        store.states[projectId] = newState;

        if (oldState == State.Campaign) {
            store.milestones[projectId][store.registry[projectId].milestone].started = currentTime;
        } else if (oldState == State.Voting && 
            store.registry[projectId].milestone < store.milestones[projectId].length) {
            store.registry[projectId].milestone = store.registry[projectId].milestone.add(1);
            store.milestones[projectId][store.registry[projectId].milestone].started = currentTime;
        }

        return oldState;
    }

    /**
     * @dev This method adds new project to registry
     * @param store Projects storage
     * @param projectId Project Id
     * @param url Link to the project description available online
     * @param urlType Type of url
     * @return bytes32 Report Id
     */
    function addReport(
        Projects storage store,
        bytes32 projectId,
        bytes32 url,
        UrlType urlType
    ) internal existedProject(store, projectId) returns (bytes32) {
        bytes32 reportId = UidsLib.createId(abi.encodePacked(url, projectId));
        store.reports[projectId]
            .milestones[store.registry[projectId].milestone]
            .push(
                Report(
                    reportId, 
                    url, 
                    urlType
                )
            );

        return reportId;
    }

    /**
     * @dev Get milestone reports Ids
     * @param store Projects storage
     * @param projectId Project Id
     * @param milestone Milestone number
     * @return address Reports list
     */
    function getReportsIds(
        Projects storage store,
        bytes32 projectId,
        uint256 milestone
    ) internal view existedProject(store, projectId) returns (bytes32[] memory) {
        bytes32[] memory ids = new bytes32[](store.reports[projectId].milestones[milestone].length);
        uint256 index = 0;

        for (uint256 i = 0; i < store.reports[projectId].milestones[milestone].length; i++) {
            ids[index] = store.reports[projectId].milestones[milestone][i].reportId;
            index += 1;
        }

        return ids;
    }

    /**
     * @dev Get milestone report by Id
     * @param store Projects storage
     * @param reportId Report Id
     * @return Report Report structure
     */
    function getReport(
        Projects storage store,
        bytes32 reportId
    ) internal view returns (Report storage) {
        bytes32 projectId = store.reportsIds[reportId];
        require(projectId != bytes32(""), "REPORT_NOT_FOUND");

        for (uint256 i = 0; i < store.milestones[projectId].length; i++) {

            for (uint256 y = 0; y < store.reports[projectId].milestones[i].length; y++) {

                if (store.reports[projectId].milestones[i][y].reportId == reportId) {
                    return store.reports[projectId].milestones[i][y];
                } 
            }
        }

        revert("REPORT_NOT_FOUND");
    }

    /**
     * @dev Get project balance
     * @param store Projects storage
     * @param projectId Project Id
     * @return uint256 Project balance
     */
    function projectBalance(
        Projects storage store, 
        bytes32 projectId
    ) internal view returns (uint256) {
        return store.balances[projectId];
    }

    /**
     * @dev Get balance of the investor
     * @param store Projects storage
     * @param projectId Project Id
     * @param investor Investor address
     * @return uint256 Project balance
     */
    function investorBalance(
        Projects storage store, 
        bytes32 projectId,
        address investor
    ) internal view existedProject(store, projectId) returns (uint256) {
        return store.investments[investor].values[projectId];
    }

    /**
     * @dev Get project investors list
     * @param store Projects storage
     * @param projectId Project Id
     * @return address[] Investor list
     */
    function projectInvestors(
        Projects storage store, 
        bytes32 projectId
    ) internal view existedProject(store, projectId) returns (address[] storage) {
        return store.investors[projectId];
    }

    /**
     * @dev Register new investment
     * @param store Projects storage
     * @param projectId Project Id
     * @param from Investor address
     * @param value Asset value to add
     */
    function registerInvestment(
        Projects storage store, 
        bytes32 projectId,
        address from,
        uint256 value
    ) internal existedProject(store, projectId) {
        uint256 newValue = store.balances[projectId].add(value);
        require(newValue <= store.registry[projectId].hardCap, "HARDCAP_REACHED");
        store.balances[projectId] = newValue;
        
        // Only new investor should be added to the list
        if (store.investments[from].values[projectId] == 0) {
            store.investors[projectId].push(from);
        }

        store.investments[from].values[projectId] = store.investments[from].values[projectId].add(value);        
    }

    /**
     * @dev Unregister investment
     * @param store Projects storage
     * @param projectId Project Id
     * @param investor Investor address
     * @param value Asset value to add
     */
    function unregisterInvestment(
        Projects storage store, 
        bytes32 projectId,
        address investor,
        uint256 value
    ) internal existedProject(store, projectId) {
        require(store.investments[investor].values[projectId] >= value, "INSUFICIENT_FUNDS");        
        store.balances[projectId] = store.balances[projectId].sub(value);
        store.investments[investor].values[projectId] = store.investments[investor].values[projectId].sub(value);

        // Remove investor from the list if his balance zeroed
        if (store.investments[investor].values[projectId] == 0) {

            address[] memory newInvestors = new address[](store.investors[projectId].length - 1);
            uint256 index = 0;

            for (uint256 i = 0; i < store.investors[projectId].length; i++) {

                if (store.investors[projectId][i] != investor) {
                    newInvestors[index] = store.investors[projectId][i];
                    index += 1;
                }
            }

            store.investors[projectId] = newInvestors;
        }
    }

    /**
     * @dev Register a vote
     * @param store Projects storage
     * @param projectId Project Id
     * @param value Vote value
     */
    function registerVote(
        Projects storage store,
        bytes32 projectId,
        uint256 value
    ) internal existedProject(store, projectId) {
        store.milestones[projectId][store.registry[projectId].milestone].votes = store.milestones[projectId][store.registry[projectId].milestone].votes.add(Voting.sqrt(value));
    }

    /**
     * @dev Calculate value of funds available to withdrawal
     * @param store Projects storage
     * @param projectId Project Id
     * @param milestoneNumber Milestone number
     * @return uint256 Amount of funds available to withdraw
     * @return uint256 Service feee value
     */
    function calculateAllowedWithdrawal(
        Projects storage store,
        bytes32 projectId,
        uint256 milestoneNumber
    ) internal view existedProject(store, projectId) returns (
        uint256 value,
        uint256 fee
    ) 
    {
        Milestone[] storage milestones = store.milestones[projectId];        
        uint256 milestonesBalance = 0;

        // Calculate actual total budget for milestones 
        for (uint256 i = milestoneNumber; i < milestones.length; i++) {
            milestonesBalance = milestonesBalance.add(milestones[i].budget);
        }

        // Part of funds above the softCap that distributed by milestones
        uint256 addFunds = store.balances[projectId]
            .sub(milestonesBalance)
            .div(milestones.length.sub(milestoneNumber));

        // Raw funds available in the current milestone
        uint256 allowedFunds = milestones[milestoneNumber].budget
            .add(addFunds)
            .sub(store.withdrawals[milestones[milestoneNumber].id]);
        
        // Service fee calculated from the fee rate
        fee = allowedFunds.mul(store.serviceController.serviceFee()).div(10000);

        // Funds available to withdraw
        value = allowedFunds.sub(fee);
    }

    /**
     * @dev Register withdrawal
     * @param store Projects storage
     * @param projectId Project Id
     * @param value Withdrawn value
     * @param fee Service fee value
     */
    function registerWithrdawal(
        Projects storage store, 
        bytes32 projectId,
        uint256 value,
        uint256 fee
    ) internal existedProject(store, projectId) {
        require(store.balances[projectId] >= value, "INSUFICIENT_FUNDS");
        bytes32 milestoneId = store.milestones[projectId][store.registry[projectId].milestone].id;
        store.balances[projectId] = store.balances[projectId].sub(value);
        store.withdrawals[milestoneId] = store.withdrawals[milestoneId].add(value);
        store.fees[projectId] = store.fees[projectId].add(fee);
    }

    /**
     * @dev This method adds new project to registry
     * @param store Projects storage
     * @param url Link to the project description available online
     * @param urlType Type of url
     * @param assetId Unique asset Id
     * @param hardCap Hard cap in assets units
     * @param startDate Start date of the fundraising campaign (unixtime)
     * @param stopDate Stop date of the fundraising campaign (unixtime)
     * @param mileDurations List of milestones durations
     * @param mileBudgets List of milestones softCaps
     * @return bytes32 Project Id
     */
    function add(
        Projects storage store,
        bytes32 url,
        UrlType urlType,
        bytes32 assetId,
        uint256 hardCap,
        uint256 startDate,
        uint256 stopDate,
        uint256[] memory mileDurations, 
        uint256[] memory mileBudgets
    ) internal returns (bytes32) {        
        return build(
            store,
            bytes32(""),
            url,
            urlType,
            assetId,
            hardCap,
            startDate,
            stopDate,
            mileDurations,
            mileBudgets
        );
    }

    /**
     * @dev This method updates project in the registry
     * @param store Projects storage
     * @param projectId Project Id (or zero bytes for creating new one)
     * @param url Link to the project description available online
     * @param urlType Type of url
     * @param assetId Unique asset Id
     * @param hardCap Hard cap in assets units
     * @param startDate Start date of the fundraising campaign (unixtime)
     * @param stopDate Stop date of the fundraising campaign (unixtime)
     * @param mileDurations List of milestones durations
     * @param mileBudgets List of milestones softCaps
     * @return bytes32 Project Id
     */
    function update(
        Projects storage store,
        bytes32 projectId,
        bytes32 url,
        ProjectsLib.UrlType urlType,
        bytes32 assetId,
        uint256 hardCap,
        uint256 startDate,
        uint256 stopDate,
        uint256[] memory mileDurations, 
        uint256[] memory mileBudgets
    ) internal existedProject(store, projectId) returns (bytes32) {
        return build(
            store, 
            projectId,
            url,
            urlType,
            assetId,
            hardCap,
            startDate,
            stopDate,
            mileDurations,
            mileBudgets
        );
    }

    /**
     * @dev This function calculate milestone dates
     * @param store Projects storage
     * @param projectId Project Id
     * @return uint256 milestoneStart
     * @return uint256 milestoneStop
     */
    function milestoneDates(
        Projects storage store,
        bytes32 projectId
    ) internal view existedProject(store, projectId) returns (
        uint256 milestoneStart, 
        uint256 milestoneStop
    ) {
        milestoneStart = store.milestones[projectId][store.registry[projectId].milestone].started;
        milestoneStop = milestoneStart.add(store.milestones[projectId][store.registry[projectId].milestone].duration);
    }

    /**
     * @dev This method creates or updates project milestones array in the storage
     * @param store Projects storage
     * @param projectId Project Id (or zero bytes for creating new one)
     * @param mileDurations List of milestones durations
     * @param mileBudgets List of milestones softCaps
     */
    function buildMilestones(
        Projects storage store,
        bytes32 projectId,
        uint256[] memory mileDurations, 
        uint256[] memory mileBudgets
    ) private {

        if (store.milestones[projectId].length > 0) {
            store.milestones[projectId].length = 0;
        }
        
        for (uint256 i = 0; i < mileDurations.length; i++) {

            store.milestones[projectId].push(
                Milestone(
                    UidsLib.createId(abi.encodePacked(i, projectId)),
                    mileDurations[i],
                    mileBudgets[i],
                    0,
                    0
                )
            );
        }
    }

    /**
     * @dev This method creates or updates project in the registry
     * @param store Projects storage
     * @param projectId Project Id (or zero bytes for creating new one)
     * @param url Link to the project description available online
     * @param urlType Type of url
     * @param assetId Unique asset Id
     * @param hardCap Hard cap in assets units
     * @param startDate Start date of the fundraising campaign (unixtime)
     * @param stopDate Stop date of the fundraising campaign (unixtime)
     * @param mileDurations List of milestones durations
     * @param mileBudgets List of milestones softCaps
     * @return bytes32 Project Id
     */
    function build(
        Projects storage store,
        bytes32 projectId,
        bytes32 url,
        UrlType urlType,
        bytes32 assetId,
        uint256 hardCap,
        uint256 startDate,
        uint256 stopDate,
        uint256[] memory mileDurations, 
        uint256[] memory mileBudgets
    ) private returns (bytes32) {
        // Project conditions validation
        // @todo Make validation process configurable
        require(url != bytes32(""), "PROJECT_DECRIPTION_REQUIRED");
        require(store.serviceController.isAsset(assetId), "ASSET_NOT_FOUND");
        require(!store.serviceController.isRemoved(assetId), "ASSET_REMOVED");
        require(stopDate > startDate, "STOPDATE_EARLIER_THEN_STARTDATE");
        require(stopDate.sub(startDate) >= 1 days, "FUNDRAISING_PERIOD_TOO_SHORT");
        require(mileDurations.length == mileBudgets.length, "MILESTONES_CONFIG_NOT_CONSISTENT");
        require(mileDurations.length > 1, "WRONG_MILESTONES_COUNT");
        
        uint256 softCap = 0;

        for (uint256 i = 0; i < mileBudgets.length; i++) {
            softCap = softCap.add(mileBudgets[i]);
        }

        require(hardCap > softCap, "HARDCAP_TOO_LOW");
        require(hardCap <= softCap.mul(2), "HARDCAP_TOO_HIGH");        
        require(mileBudgets[0] <= softCap.div(mileDurations.length), "FIRST_MILESTONE_BUDGET_TOO_HIGH");

        bool isUpdate = projectId != bytes32("") ? true : false;
        projectId = isUpdate ? projectId : UidsLib.createId(abi.encodePacked(url, projectId, assetId));

        if (!isUpdate) {
            require(!isProject(store, projectId), "PROJECT_ALREADY_EXISTED");
            store.ids.push(projectId);
        }

        store.registry[projectId] = Project(
            msg.sender,
            projectId,
            url,
            urlType,
            assetId,
            softCap,
            hardCap,
            startDate,
            stopDate,
            0
        );

        // Build milestones structures and add them to the store
        buildMilestones(store, projectId, mileDurations, mileBudgets);

        // Set the initial project state
        store.states[projectId] = State.Uninitialized;

        return projectId;
    }
}
