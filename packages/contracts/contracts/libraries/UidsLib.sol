pragma solidity 0.5.11;

import "openzeppelin-solidity/contracts/math/SafeMath.sol";


/**
 * @title Unique hashes library
 * @dev This library creates and operates unique hashes that used as ids in other registries
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 */
library UidsLib {

    using SafeMath for uint256;

    /**
     * @dev Returns unique Id
     * @param solt Solt string
     * @return bytes32 Unique hash
     */
    function createId(bytes memory solt) internal view returns (bytes32) {
        return keccak256(
            abi.encodePacked(
                solt,
                msg.sender,
                gasleft(),
                block.gaslimit,
                block.difficulty,
                msg.sig,
                blockhash(block.number.sub(1))
            )
        ); 
    }
}
