pragma solidity 0.5.11;

import "./UidsLib.sol";


/**
 * @title Assets Library
 * @dev This library holds assets data structures and management logic
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 */
library AssetsLib {

    /**
     * @dev List of acceptable assets types
     * Native type means - ETH
     */
    enum AssetType {
        Native,
        Erc20
    }

    /**
     * @dev Asset configuration
     * @param id Unique asset Id
     * @param assetType Asset type
     * @param assetAddress Asset contract address
     * @param nativeFactor Tokens multiplier factor in relation to the ETH (multiplied by 1000000)
     */
    struct Asset {
        bytes32 id;
        AssetType assetType;
        address assetAddress;
        uint256 nativeFactor;
        bool removed;
    }

    /**
     * @dev Assets registry storage
     * @param registry Assets registry 
     * @param ids Assets Ids existance
     * @param assets All assets Ids list
     */
    struct Assets {
        mapping (bytes32 => Asset) registry;// assetId => Asset
        bytes32[] ids;
    }

    /**
     * @dev This modifier allows function execution if provided asset is existed
     * @param store Assets storage
     * @param id Asset Id
     */
    modifier existedOnly(Assets storage store, bytes32 id) {
        require(store.registry[id].id != bytes32(""), "ASSET_NOT_FOUND");
        _;
    }

    /**
     * @dev This modifier allows function execution if provided asset has not been removed
     * @param store Assets storage
     * @param id Asset Id
     */
    modifier notRemovedOnly(Assets storage store, bytes32 id) {
        require(!store.registry[id].removed, "ASSET_REMOVED");
        _;
    }

    /**
     * @dev Check is asset exists
     * @param store Assets storage
     * @param id Asset Id
     * @return bool Asset existance
     */
    function isAsset(
        Assets storage store,
        bytes32 id
    ) internal view returns (bool) {
        return store.registry[id].id != bytes32("");
    }

    /**
     * @dev This method will add new asset to storage
     * @param store Assets storage
     * @param assetType Asset type
     * @param assetAddress Asset contract address
     * @param nativeFactor Tokens multiplier factor
     * @return bytes32 Asset Id
     */
    function add(
        Assets storage store,
        AssetType assetType,
        address assetAddress,
        uint256 nativeFactor
    ) internal returns (bytes32) {
        AssetsLib.Asset memory asset = build(bytes32(""), assetType, assetAddress, nativeFactor);
        require(!isAsset(store, asset.id), "ASSET_ALREADY_EXISTED");
        store.registry[asset.id] = asset;
        store.ids.push(asset.id);
        return asset.id;
    }

    /**
     * @dev This method will update asset contract address
     * @param store Assets storage
     * @param id Asset id
     * @param assetAddress Asset contract address
     * @param nativeFactor Tokens multiplier factor
     */
    function update(
        Assets storage store,
        bytes32 id,
        address assetAddress,
        uint256 nativeFactor
    ) internal existedOnly(store, id) notRemovedOnly(store, id) {
        store.registry[id] = build(
            id, 
            store.registry[id].assetType, 
            assetAddress, 
            nativeFactor
        );
    }

    /**
     * @dev This method will remove asset from the registry
     * @param store Assets storage
     * @param id Asset id
     */
    function remove(
        Assets storage store,
        bytes32 id
    ) internal existedOnly(store, id) notRemovedOnly(store, id) {
                
        // Asset will be removed from the list of actual assets but 
        // not removed from registry for remote contracts data consistency
        // Removed asset only marked as "removed"
        store.registry[id].removed = true;
        bytes32[] memory newAssetsIds = new bytes32[](store.ids.length - 1);
        uint256 index = 0;

        for (uint256 i = 0; i < store.ids.length; i++) {

            if (store.ids[i] != id) {
                newAssetsIds[index] = store.ids[i];
                index += 1;
            }
        }

        store.ids = newAssetsIds;
    }

    /**
     * @dev Check asset removed status
     * @param store Assets storage
     * @param id Asset id
     * @return assetType Asset type
     * @return assetAddress Asset contract address
     * @return nativeFactor Tokens multiplier factor
     */
    function isRemoved(
        Assets storage store,
        bytes32 id
    ) internal view existedOnly(store, id) returns (bool) {
        return store.registry[id].removed;
    }

    /**
     * @dev This method will return asset configuration from the registry
     * @param store Assets storage
     * @param id Asset id
     * @return assetType Asset type
     * @return assetAddress Asset contract address
     * @return nativeFactor Tokens multiplier factor
     */
    function get(
        Assets storage store,
        bytes32 id
    ) internal view existedOnly(store, id) returns (
        AssetType assetType,
        address assetAddress,
        uint256 nativeFactor,
        bool removed
    ) {
        assetType = store.registry[id].assetType;
        assetAddress = store.registry[id].assetAddress;
        nativeFactor = store.registry[id].nativeFactor;
        removed = store.registry[id].removed;
    }

    /**
     * @dev This method will create asset configuration structure
     * @param id Asset Id. Should be set to bytes32("") if new Id generation required
     * @param assetType Asset type
     * @param assetAddress Asset contract address
     * @param nativeFactor Tokens multiplier factor
     * @return AssetsLib.Asset Asset structure
     */
    function build(
        bytes32 id,
        AssetType assetType,
        address assetAddress,
        uint256 nativeFactor
    ) private view returns(Asset memory) {
        require(nativeFactor >= 1, "NATIVE_FACTOR_TOO_SMALL");

        if (assetType == AssetType.Native && assetAddress != address(0)) {
            revert("NATIVE_TYPE_REQUIRE_ZERO_ADDRESS");
        } else if (assetType != AssetType.Native && assetAddress == address(0)) {
            revert("NON_ZERO_ADDRESS_REQUIRED");
        }

        return Asset(
            id != bytes32("") ? id : UidsLib.createId(abi.encodePacked(assetAddress)), 
            assetType, 
            assetAddress, 
            nativeFactor,
            false
        );
    }         
}
