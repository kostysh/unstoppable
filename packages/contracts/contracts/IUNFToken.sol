pragma solidity 0.5.11;

import "openzeppelin-solidity/contracts/token/ERC20/IERC20.sol";


contract IUNFToken is IERC20 {
    function mint(address to, uint256 value) external;
    function burn(address from, uint256 value) external;
}