pragma solidity 0.5.11;

import "../IUnstoppable.sol";


contract UnstoppableReentracyTests {

    IUnstoppable private target;
    bytes32 private assetId;
    uint256 private value;

    function() external payable {
        target.withdraw(assetId, value);
    }

    function withdrawEther(address _instance, bytes32 _assetId, uint256 _value) external {
        target = IUnstoppable(_instance);
        assetId = _assetId;
        value = _value;
        target.withdraw(assetId, value);
    }

    function withdrawErc20(address _instance, bytes32 _assetId, uint256 _value) external {
        target = IUnstoppable(_instance);
        assetId = _assetId;
        value = _value;
        target.withdraw(assetId, value);

        (bool success, ) = address(this).call("");

        if (!success) {
            revert();
        }
    }
}
