pragma solidity 0.5.11;

import "../IUNFToken.sol";


contract UNFTokenCaller {

    IUNFToken private token;

    constructor (IUNFToken _token) public {
        token = _token;
    }
    
    function mint(address to, uint256 value) external {
        return token.mint(to, value);
    }

    function burn(address from, uint256 value) external {
        return token.burn(from, value);
    }
}