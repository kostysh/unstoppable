pragma solidity 0.5.11;

import "openzeppelin-solidity/contracts/introspection/ERC165Checker.sol";


contract ERC165CheckerTests {

    function supportsInterface(address _instance, bytes4 _interface) external view {
        require(
            ERC165Checker._supportsInterface(_instance, _interface), 
            "INTERFACE_NOT_SUPPORTED"
        );
    }
}
