pragma solidity 0.5.11;

import "../registries/ProjectsRegistry.sol";


contract ProjectRegistryReentracyTests {

    ProjectsRegistry private target;
    bytes private funcToCall;
    uint256 private value;

    function() external payable {
        (bool success, ) = address(target).call.value(value)(funcToCall);

        if (!success) {
            revert();
        }
    }

    function voteReentracy(
        address _instance, 
        bytes32 _projectId, 
        uint256 _value
    ) external {
        target = ProjectsRegistry(_instance);
        funcToCall = abi.encodeWithSignature(
            "vote(bytes32,uint256)", 
            _projectId,
            _value
        );
        target.vote(_projectId, _value);
        
        // reentracy via fallback function
        (bool success, ) = address(this).call("");

        if (!success) {
            revert();
        }
    }
}