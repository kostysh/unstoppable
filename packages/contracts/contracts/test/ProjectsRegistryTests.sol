pragma solidity 0.5.11;

import "../registries/ProjectsRegistry.sol";


contract ProjectsRegistryTests is ProjectsRegistry {
    uint256 private currentTime;

    constructor (address _serviceController, address _token) 
        ProjectsRegistry(_serviceController, _token) public {} 

    function setCurrentTime(uint256 time) external {
        currentTime = time;
    }

    function time() internal view returns (uint256) {
        return currentTime == 0 ? now : currentTime;// solhint-disable-line not-rely-on-time
    }
}