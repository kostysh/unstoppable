pragma solidity 0.5.11;

import "openzeppelin-solidity/contracts/introspection/ERC165.sol";
import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "openzeppelin-solidity/contracts/token/ERC20/ERC20Detailed.sol";
import "openzeppelin-solidity/contracts/access/roles/WhitelistedRole.sol";
import "openzeppelin-solidity/contracts/utils/ReentrancyGuard.sol";


/**
 * @title Investment tokens for the Unstoppable Fundraising Platform (UFP) contract
 * @dev This contract holds main logic and storages 
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 */
contract UNFToken is ERC20, ERC20Detailed, ERC165, ReentrancyGuard, WhitelistedRole {

    /**
     * @dev This event will be emitted when tokens are minted
     * @param minter Tokens minter
     * @param recipient Tokens recipient
     * @param value Minted tokens value
     */
    event Minted(
        address minter,
        address recipient,
        uint256 value
    );

    /**
     * @dev This event will be emitted when tokens are burned
     * @param burner Tokens burner
     * @param owner Tokens owner
     * @param value Burned tokens value
     */
    event Burned(
        address burner,
        address owner,
        uint256 value
    );
    
    /**
     * @dev Contract constructor
     * @param owner UFP owner
     * @param initialSupply Initial amount of tokens minted for owner
     */
    constructor(address owner, uint256 initialSupply) 
        ERC20Detailed("Investment Token of the Unstoppable Fundraising Platform", "UFP", 18) 
        public 
    {
        _registerInterface(getInterfaceId());
        
        if (initialSupply > 0) {
            mintTo(owner, initialSupply);
        }
    }

    /**
     * @dev Calculating the Interface ID for the token
     * @return bytes4 ERC165 Interface ID
     */
    function getInterfaceId() public pure returns (bytes4) {
        ERC20 i;
        // 0x36372b07
        return i.totalSupply.selector ^
            i.balanceOf.selector ^
            i.transfer.selector ^
            i.allowance.selector ^
            i.approve.selector ^
            i.transferFrom.selector;
    }

    /**
     * @dev Mint and transfer tokens to the recipient.
     * This function can be called by the whitelisted address only
     * @param to Tokens recipient
     * @param value Amount of tokens to mint
     */
    function mint(address to, uint256 value) external onlyWhitelisted nonReentrant {
        mintTo(to, value);
    }

    /**
     * @dev Burn tokens.
     * This function can be called by the whitelisted address only
     * @param from Tokens recipient
     * @param value Amount of tokens to mint
     */
    function burn(address from, uint256 value) external onlyWhitelisted nonReentrant {
        burnFrom(from, value);
    }

    /**
     * @dev Mint tokens
     * @param to Tokens recipient
     * @param value Amount of tokens to mint
     */
    function mintTo(address to, uint256 value) internal {
        require(value > 0, "ZERO_VALUE_NOT_ALLOWED");
        _mint(to, value);
        emit Minted(msg.sender, to, value);
    }

    /**
     * @dev Burn tokens
     * @param from Tokens recipient
     * @param value Amount of tokens to mint
     */
    function burnFrom(address from, uint256 value) internal {
        require(value > 0, "ZERO_VALUE_NOT_ALLOWED");
        _burn(from, value);
        emit Burned(msg.sender, from, value);
    }
}
