pragma solidity 0.5.11;

import "./registries/IAssetsRegistry.sol";


contract IUnstoppable is IAssetsRegistry {
    function serviceFee() external view returns (uint256);
    function calculateTokens(
        uint256 investmentValue,
        uint256 softCap,
        uint256 nativeFactor
    ) external pure returns (uint256);
    function paused() public view returns (bool);
    function withdraw(
        bytes32 assetId,
        uint256 value
    ) external;
}
