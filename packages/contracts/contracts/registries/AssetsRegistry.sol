pragma solidity 0.5.11;

import "openzeppelin-solidity/contracts/introspection/ERC165.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/lifecycle/Pausable.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "./IAssetsRegistry.sol";
import "../libraries/AssetsLib.sol";


/**
 * @title Assets registry
 * @dev This contract holds assets configuration 
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 */
contract AssetsRegistry is IAssetsRegistry, ERC165, Ownable, Pausable {

    using SafeMath for uint256;
    using AssetsLib for AssetsLib.Assets;
    
    /// @dev Assets storage
    AssetsLib.Assets internal assets;

    /**
     * @dev This event will be emitted when new asset is added
     * @param id Asset Id
     * @param creator Assets admin
     */
    event AssetAdded(
        bytes32 id,
        address creator
    );

    /**
     * @dev This event will be emitted when new asset is updated
     * @param id Asset Id
     * @param addr New asset contract address
     * @param creator Assets admin
     */
    event AssetUpdated(
        bytes32 id,
        address addr,
        address creator
    );

    /**
     * @dev This event will be emitted when the asset is removed
     * @param id Asset Id
     * @param remover Assets admin
     */
    event AssetRemoved(
        bytes32 id,
        address remover
    );

    /**
     * @dev Assets registry constructor
     */
    constructor() public {
        _registerInterface(getInterfaceId());
    }

    /**
     * @dev This method will add new asset to registry
     * @param assetType Asset type
     * @param assetAddress Asset contract address
     * @param nativeFactor Tokens multiplier factor
     */
    function addAsset(
        AssetsLib.AssetType assetType,
        address assetAddress,
        uint256 nativeFactor
    ) external onlyOwner {
        bytes32 assetId = assets.add(assetType, assetAddress, nativeFactor);        
        emit AssetAdded(assetId, msg.sender);
    }

    /**
     * @dev This method will update asset contract address
     * @param assetId Asset id
     * @param assetAddress Asset contract address
     * @param nativeFactor Tokens multiplier factor
     */
    function updateAsset(
        bytes32 assetId,
        address assetAddress,
        uint256 nativeFactor
    ) external onlyOwner whenPaused {
        assets.update(assetId, assetAddress, nativeFactor);
        emit AssetUpdated(assetId, assetAddress, msg.sender);
    }

    /**
     * @dev This method will remove asset from the registry
     * @param assetId Asset id
     */
    function removeAsset(bytes32 assetId) external onlyOwner whenPaused {
        assets.remove(assetId);
        emit AssetRemoved(assetId, msg.sender);
    }

    /**
     * @dev This method will return asset existance
     * @param assetId Asset id
     * @return bool Asset existance
     */
    function isAsset(bytes32 assetId) external view returns (bool) {
        return assets.isAsset(assetId);
    }

    /**
     * @dev This method will return asset removed status
     * @param assetId Asset id
     * @return bool Asset removed status
     */
    function isRemoved(bytes32 assetId) external view returns (bool) {
        return assets.isRemoved(assetId);
    }

    /**
     * @dev This method will return list of assets
     * @return bytes32[] List of assets Ids
     */
    function getAssets() external view returns (bytes32[] memory) {
        return assets.ids;
    }

    /**
     * @dev This method will return asset configuration from the registry
     * @param assetId Asset id
     * @return AssetsLib.AssetType Asset type
     * @return address Asset contract address
     * @return uint256 Tokens multiplier factor
     */
    function getAsset(bytes32 assetId) external view returns (
        AssetsLib.AssetType assetType,
        address assetAddress,
        uint256 nativeFactor,
        bool removed
    ) {
        (assetType, assetAddress, nativeFactor, removed) = assets.get(assetId);
    }

    /**
     * @dev Calculating the Interface ID for the AssetsRegistry
     * @return bytes4 ERC165 Interface ID
     */
    function getInterfaceId() public pure returns (bytes4) {
        IAssetsRegistry i;
        // 0x365efc2a
        return i.isAsset.selector ^
            i.getAsset.selector;
    }
}
