pragma solidity 0.5.11;


/**
 * @title Projects registry interface
 * @dev This contract holds ProjectsRegistry functions interface 
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 */
contract IProjectsRegistry {
    function isProject(bytes32 projectId) external view returns (bool);
}