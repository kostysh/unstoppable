pragma solidity 0.5.11;

import "openzeppelin-solidity/contracts/introspection/ERC165.sol";
import "openzeppelin-solidity/contracts/introspection/ERC165Checker.sol";
import "openzeppelin-solidity/contracts/token/ERC20/IERC20.sol";
import "openzeppelin-solidity/contracts/token/ERC20/SafeERC20.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/utils/ReentrancyGuard.sol";
import "./IProjectsRegistry.sol";
import "../libraries/ProjectsLib.sol";
import "./IAssetsRegistry.sol";
import "../IUNFToken.sol";
import "../IUnstoppable.sol";


/**
 * @title Projects registry
 * @dev This contract holds projects storage and logic 
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 */
contract ProjectsRegistry is IProjectsRegistry, ERC165, ReentrancyGuard {

    using SafeMath for uint256;
    using SafeERC20 for IERC20;
    using ProjectsLib for ProjectsLib.Projects;

    bytes4 private constant _INTERFACE_ID_SERVICE_CONTROLLER = 0x3404764d;
    bytes4 private constant _INTERFACE_ID_TOKEN = 0x36372b07;

    /// @dev UNF token instance
    IUNFToken private token;

    /// @dev Projects storage
    ProjectsLib.Projects private projects;

    /**
     * @dev This event will be emitted when new project is added
     * @param id Project Id
     * @param owner Project owner
     */
    event ProjectAdded(
        bytes32 id,
        address owner
    );

    /**
     * @dev This event will be emitted when new project is updated
     * @param id Project Id
     * @param owner Project owner
     */
    event ProjectUpdated(
        bytes32 id,
        address owner
    );

    /**
     * @dev This event will be emitted when the project state has been changed
     * @param id Project Id
     * @param newState New project state
     * @param oldState Old project state
     */
    event ProjectStateChanged(
        bytes32 id,
        ProjectsLib.State newState,
        ProjectsLib.State oldState
    );

    /**
     * @dev This event will be emitted when investor made investment
     * @param id Project Id
     * @param investor Ivestors address
     * @param value Investment value
     */
    event Investment(
        bytes32 id,
        address investor,
        uint256 value
    );

    /**
     * @dev This event will be emitted when investor made a rage quit
     * @param id Project Id
     * @param investor Ivestors address
     * @param value Investment value
     */
    event RageQuit(
        bytes32 id,
        address investor,
        uint256 value
    );

    /**
     * @dev This event will be emitted when the project owner did withdrawal
     * @param id Project Id
     * @param owner Ivestors address
     * @param value Investment value
     * @param fee Service fee value
     */
    event Withdraw(
        bytes32 id,
        address owner,
        uint256 value,
        uint256 fee
    );

    /**
     * @dev This event will be emitted when the project milestone report has been published
     * @param id Project Id
     * @param reportId Report Id
     * @param owner Report publisher
     */
    event ReportPublished(
        bytes32 id,
        bytes32 reportId,
        address owner
    );

    /**
     * @dev This event will be emitted when investor make a vote
     * @param id Project Id
     * @param investor Project investor
     * @param value Vote value
     */
    event Voted(
        bytes32 id,
        address investor,
        uint256 value
    );

    /**
     * @dev Projects registry constructor
     * @param _serviceController Service controller instance address
     * @param _token Utility token instance address
     */
    constructor(address _serviceController, address _token) public {
        _registerInterface(getInterfaceId());

        require(
            ERC165Checker._supportsInterface(_serviceController, _INTERFACE_ID_SERVICE_CONTROLLER), 
            "NOT_SUPPORTED_CONTROLLER_INTERFACE"
        );
        projects.serviceController = IUnstoppable(_serviceController);

        require(
            ERC165Checker._supportsInterface(_token, _INTERFACE_ID_TOKEN), 
            "NOT_SUPPORTED_TOKEN_INTERFACE"
        );
        token = IUNFToken(_token);
        
        // Initialize project transitions table
        projects.transitionsTable[uint256(ProjectsLib.State.Uninitialized)] = [
            ProjectsLib.State.Created
        ];
        projects.transitionsTable[uint256(ProjectsLib.State.Created)] = [
            ProjectsLib.State.Closed, 
            ProjectsLib.State.Campaign
        ];
        projects.transitionsTable[uint256(ProjectsLib.State.Closed)] = [
            ProjectsLib.State.Closed
        ];
        projects.transitionsTable[uint256(ProjectsLib.State.Campaign)] = [
            ProjectsLib.State.CampaignFailed, 
            ProjectsLib.State.Started
        ];
        projects.transitionsTable[uint256(ProjectsLib.State.CampaignFailed)] = [
            ProjectsLib.State.Closed
        ];
        projects.transitionsTable[uint256(ProjectsLib.State.Started)] = [
            ProjectsLib.State.Voting,
            ProjectsLib.State.Finished
        ];
        projects.transitionsTable[uint256(ProjectsLib.State.Voting)] = [
            ProjectsLib.State.VotingFailed, 
            ProjectsLib.State.Started
        ];
        projects.transitionsTable[uint256(ProjectsLib.State.VotingFailed)] = [
            ProjectsLib.State.Closed
        ];
        projects.transitionsTable[uint256(ProjectsLib.State.Finished)] = [
            ProjectsLib.State.Closed
        ];
    }

    /**
     * @dev This modifier allows function execution if service not paused only
     */
    modifier whenNotPaused() {
        require(!projects.serviceController.paused(), "SERVICE_PAUSED");
        _;
    }

    /**
     * @dev This modifier allows function execution to project owner only
     * @param projectId Project Id
     */
    modifier onlyProjectOwner(bytes32 projectId) {
        require(projects.getOwner(projectId) == msg.sender, "NOT_AN_OWNER");
        _;
    }

    /**
     * @dev This modifier allows function execution to project investor only
     * @param projectId Project Id
     */
    modifier onlyProjectInvestor(bytes32 projectId) {
        require(projects.investorBalance(projectId, msg.sender) > 0, "NOT_AN_INVESTOR");
        _;
    }

    /**
     * @dev This modifier allows function execution in the specific project state only
     * @param projectId Project Id
     * @param requiredState Required project state
     */
    modifier onlyState(
        bytes32 projectId, 
        ProjectsLib.State requiredState
    ) {
        require(projects.getState(projectId) == requiredState, "NOT_ALLOWED_IN_CURRENT_STATE");
        _;
    }

    /**
     * @dev This modifier allows function execution if project started only
     * @param projectId Project Id
     */
    modifier onlyStarted(bytes32 projectId) {
        require(projects.isStarted(projectId, time()), "PROJECT_NOT_STARTED");
        _;
    }

    /**
     * @dev This modifier allows function execution if project not started
     * @param projectId Project Id
     */
    modifier notStarted(bytes32 projectId) {
        require(projects.isNotStarted(projectId, time()), "PROJECT_STARTED");
        _;
    }

    /**
     * @dev This modifier allows function execution if project not expired
     * @param projectId Project Id
     */
    modifier notExpired(bytes32 projectId) {
        require(projects.isExpired(projectId, time()), "PROJECT_EXPIRED");
        _;
    }

    /**
     * @dev Check for project existance
     * @param projectId Project Id
     * @return bool
     */
    function isProject(bytes32 projectId) external view returns (bool) {
        return projects.isProject(projectId);
    }

    /**
     * @dev This method adds new project to registry
     * @param url Link to the project description available online
     * @param urlType Type of url
     * @param assetId Unique asset Id
     * @param hardCap Hard cap in assets units
     * @param startDate Start date of the fundraising campaign (unixtime)
     * @param stopDate Stop date of the fundraising campaign (unixtime)
     * @param mileDurations List of milestones durations
     * @param mileBudgets List of milestones softCaps
     */
    function addProject(
        bytes32 url,
        ProjectsLib.UrlType urlType,
        bytes32 assetId,
        uint256 hardCap,
        uint256 startDate,
        uint256 stopDate,
        uint256[] calldata mileDurations, 
        uint256[] calldata mileBudgets
    ) external whenNotPaused {        
        bytes32 projectId = projects.add(
            url,
            urlType,
            assetId,
            hardCap,
            startDate,
            stopDate,
            mileDurations,
            mileBudgets
        );
        moveProjectToState(projectId, ProjectsLib.State.Created);
        emit ProjectAdded(projectId, msg.sender);
    }

    /**
     * @dev This method updates project in the registry
     * @param projectId Project Id (or zero bytes for creating new one)
     * @param url Link to the project description available online
     * @param urlType Type of url
     * @param assetId Unique asset Id
     * @param hardCap Hard cap in assets units
     * @param startDate Start date of the fundraising campaign (unixtime)
     * @param stopDate Stop date of the fundraising campaign (unixtime)
     * @param mileDurations List of milestones durations
     * @param mileBudgets List of milestones budgets
     */
    function updateProject(
        bytes32 projectId,
        bytes32 url,
        ProjectsLib.UrlType urlType,
        bytes32 assetId,
        uint256 hardCap,
        uint256 startDate,
        uint256 stopDate,
        uint256[] calldata mileDurations, 
        uint256[] calldata mileBudgets
    ) 
        external 
        whenNotPaused 
        onlyProjectOwner(projectId) 
        onlyState(projectId, ProjectsLib.State.Created)     
    {
        projects.update(
            projectId,
            url,
            urlType,
            assetId,
            hardCap,
            startDate,
            stopDate,
            mileDurations,
            mileBudgets
        );        
        emit ProjectUpdated(projectId, msg.sender);
    }
    
    /**
     * @dev Starts project funding campaign
     * @param projectId Project Id
     */
    function startCampaign(bytes32 projectId) 
        external 
        whenNotPaused 
        onlyProjectOwner(projectId) 
        notStarted(projectId) 
        notExpired(projectId)
    {
        moveProjectToState(projectId, ProjectsLib.State.Campaign);
    }

    /**
     * @dev Starts project 
     * @param projectId Project Id
     */
    function startProject(bytes32 projectId) 
        external 
        whenNotPaused 
        onlyProjectOwner(projectId) 
        onlyState(projectId, ProjectsLib.State.Campaign)
        notExpired(projectId)  
    {
        ( , , , , uint256 softCap, , uint256 startDate, , ) = getProject(projectId);
        
        if (projects.projectBalance(projectId) < softCap) {

            if (time() >= startDate) {
                moveProjectToState(projectId, ProjectsLib.State.CampaignFailed);
            } else {
                revert("SOFTCAP_NOT_REACHED");
            }
            
        } else  {
            moveProjectToState(projectId, ProjectsLib.State.Started);
        }        
    }

    /**
     * @dev Close the project
     * @param projectId Project Id
     */
    function closeProject(bytes32 projectId) 
        external 
        whenNotPaused 
        onlyProjectOwner(projectId) 
    {
        moveProjectToState(projectId, ProjectsLib.State.Closed);
    }

    /**
     * @dev Finish the project
     * @param projectId Project Id
     */
    function finishProject(bytes32 projectId) 
        external 
        whenNotPaused 
        onlyProjectOwner(projectId) 
        onlyState(projectId, ProjectsLib.State.Started)
    {
        require(projects.isFinalMilestone(projectId), "NOT_A_FINAL_MILESTONE");
        moveProjectToState(projectId, ProjectsLib.State.Finished);
    }

    /**
     * @dev Publishing of the project milestone report
     * @param projectId Project Id
     * @param url Link to the project milestone report available online
     * @param urlType Type of url
     */
    function publishReport(
        bytes32 projectId,
        bytes32 url,
        ProjectsLib.UrlType urlType
    ) 
        external 
        whenNotPaused 
        onlyProjectOwner(projectId) 
        onlyState(projectId, ProjectsLib.State.Started)
    {
        // @todo Add condition of required withdrawal (allowed withdrawal value should be zero)
        ProjectsLib.State currentState = projects.getState(projectId);
        require(currentState == ProjectsLib.State.Started || 
            currentState == ProjectsLib.State.Voting, "NOT_ALLOWED_IN_CURRENT_STATE");
        bytes32 reportId = projects.addReport(
            projectId,
            url,
            urlType
        );
        
        if (!projects.isFinalMilestone(projectId) && 
            projects.getState(projectId) != ProjectsLib.State.Voting) {
            
            moveProjectToState(projectId, ProjectsLib.State.Voting);
        }

        emit ReportPublished(projectId, reportId, msg.sender);
    }

    /**
     * @dev Vote for the current project milestone
     * @param projectId Project Id
     * @param value Number of tokens
     */
    function vote(
        bytes32 projectId,
        uint256 value
    ) 
        external 
        whenNotPaused 
        nonReentrant 
        onlyProjectInvestor(projectId) 
        onlyState(projectId, ProjectsLib.State.Voting)
    {
        require(token.balanceOf(msg.sender) >= value, "INSUFICIENT_FUNDS");
        token.burn(msg.sender, value);
        projects.registerVote(projectId, value); 
        emit Voted(projectId, msg.sender, value);
    }

    /**
     * @dev Close voting stage
     * @param projectId Project Id
     * @return bool Voting result: true - success, false - failed
     */
    function closeVoting(bytes32 projectId) 
        public 
        whenNotPaused
        onlyProjectOwner(projectId) 
        onlyState(projectId, ProjectsLib.State.Voting) 
        returns (bool)
    {   
        // @todo Add condition about votig time frame (min a nd max voting value)     
        (uint256 votes, uint256 threshold) = votingResult(projectId);

        if (votes >= threshold) {
            moveProjectToState(projectId, ProjectsLib.State.Started);
            return true;
        } else {
            moveProjectToState(projectId, ProjectsLib.State.VotingFailed);
            return false;
        }    
    }

    /**
     * @dev Make investment
     * @param projectId Project Id
     * @param value Imvestment value
     */
    function invest(
        bytes32 projectId, 
        uint256 value
    ) 
        external 
        payable 
        whenNotPaused 
        nonReentrant 
        onlyState(projectId, ProjectsLib.State.Campaign) 
        notStarted(projectId) 
        notExpired(projectId) 
    {
        doInvestment(projectId, msg.sender, value);
    }

    /**
     * @dev Make delegated investment
     * @param projectId Project Id
     * @param from Delegated sender address
     * @param value Imvestment value
     */
    function investFrom(
        bytes32 projectId, 
        address from,
        uint256 value
    ) 
        external 
        payable 
        whenNotPaused 
        nonReentrant 
        onlyState(projectId, ProjectsLib.State.Campaign) 
        notStarted(projectId) 
        notExpired(projectId) 
    {
        doInvestment(projectId, from, value);
    }

    /**
     * @dev Rage quit
     * @param projectId Project Id
     * @param value Amount of investment value to quit
     */
    function rageQuit(
        bytes32 projectId,
        uint256 value
    ) 
        external 
        nonReentrant 
        onlyProjectInvestor(projectId)
    {
        require(msg.sender != address(0), "WRONG_SENDER_ADDRESS");
        require(projects.isRageQuitAllowed(projectId, time()), "RAGE_QUIT_NOT_ALLOWED");
        projects.unregisterInvestment(projectId, msg.sender, value);
        transferAsset(projectId, address(this), msg.sender, value);
        emit RageQuit(projectId, msg.sender, value);
    }

    /**
     * @dev Withdraw project funds
     * @param projectId Project Id
     */
    function withdraw(
        bytes32 projectId
    ) 
        external 
        whenNotPaused 
        nonReentrant 
        onlyProjectOwner(projectId) 
        onlyState(projectId, ProjectsLib.State.Started)        
    {        
        (uint256 value, uint256 fee) = projects.calculateAllowedWithdrawal(
            projectId, 
            projects.getCurrentMilestone(projectId)
        );
        projects.registerWithrdawal(projectId, value, fee);
        
        transferAsset(projectId, address(this), msg.sender, value);
        transferAsset(projectId, address(this), address(uint160(address(projects.serviceController))), fee);

        mintTokensForInvestors(projectId, value);

        emit Withdraw(projectId, msg.sender, value, fee);
    }

    /**
     * @dev Return voting result for the current milestone
     * @param projectId Project Id
     * @return uint256 value Accepted votes
     * @return uint256 threshold Votes value required to success
     */
    function votingResult(bytes32 projectId) 
        public 
        view 
        onlyState(projectId, ProjectsLib.State.Voting)
        returns (
            uint256 value,
            uint256 threshold
        ) 
    {
        return projects.getVotingResult(projectId);
    }

    /**
     * @dev Calculate amount of funds available to withdraw and service fee value
     * @param projectId Project Id
     * @return uint256 Amount of funds available to withdraw
     * @return uint256 Service fee value
     */
    function calculateWithdraw(bytes32 projectId) 
        public 
        view 
        returns (
            uint256 value, 
            uint256 fee
        ) 
    {
        return projects.calculateAllowedWithdrawal(
            projectId, 
            projects.getCurrentMilestone(projectId)
        );        
    }

    /**
     * @dev Get project by Id
     * @param projectId Project Id
     * @return address Project owner address
     * @return bytes32 Project description url 
     * @return uint256 Project description url type
     * @return bytes32 Asset Id
     * @return uint256 Project soft cap
     * @return uint256 Project hard cap
     * @return uint256 Project start date
     * @return uint256 Project stop date
     * @return uint256 Current milestone 
     */
    function getProject(bytes32 projectId) public view returns (
        address owner,
        bytes32 url,
        uint256 urlType,
        bytes32 assetId,
        uint256 softCap,
        uint256 hardCap,
        uint256 startDate,
        uint256 stopDate,
        uint256 milestone
    ) {
        ProjectsLib.Project memory project = projects.getProject(projectId);
        owner = project.owner;
        url = project.url;
        urlType = uint256(project.urlType);
        assetId = project.assetId;
        softCap = project.softCap;
        hardCap = project.hardCap;
        startDate = project.startDate;
        stopDate = project.stopDate;
        milestone = project.milestone;
    }

    /**
     * @dev Get a balance of the project
     * @param projectId Project Id
     * @param investor Investor address
     * @return uint256 Project balance
     */
    function investorBalance(
        bytes32 projectId,
        address investor
    ) public view returns (uint256) {
        require(projects.isProject(projectId), "PROJECT_NOT_FUND");
        return projects.investorBalance(projectId, investor);
    }

    /**
     * @dev Get a balance of the project
     * @param projectId Project Id
     * @return uint256 Project balance
     */
    function projectBalance(bytes32 projectId) public view returns (uint256) {
        require(projects.isProject(projectId), "PROJECT_NOT_FUND");
        return projects.projectBalance(projectId);
    }

    /**
     * @dev Get a current state of the project
     * @param projectId Project Id
     * @return ProjectsLib.State Project state
     */
    function projectState(bytes32 projectId) public view returns (ProjectsLib.State) {
        return projects.getState(projectId);
    }

    /**
     * @dev Get a milestones count of the project
     * @param projectId Project Id
     * @return uint256 Milestones count
     */
    function projectMilestonesCount(bytes32 projectId) public view returns (uint256) {
        return projects.getMilestones(projectId).length;
    }

    /**
     * @dev Calculating the Interface ID for the ProjectsRegistry
     * @return bytes4 ERC165 Interface ID
     */
    function getInterfaceId() public pure returns (bytes4) {
        IProjectsRegistry i;
        // 0xffaef45d
        return bytes4(i.isProject.selector);
    }

    /**
     * @dev Get current time. 
     * This function can be overriden for testing purposes
     * @return uint256 Current block time
     */
    function time() internal view returns (uint256) {
        return now;// solhint-disable-line not-rely-on-time
    }

    /**
     * @dev Mint investment tokens for investors
     * @param projectId Project Id
     * @param value Amount of funds to withdraw
     */
    function mintTokensForInvestors(
        bytes32 projectId,
        uint256 value
    ) internal {
        ProjectsLib.Project storage project = projects.getProject(projectId);
        ( , , uint256 nativeFactor, ) = projects.serviceController.getAsset(project.assetId);
        address[] storage investors = projects.projectInvestors(projectId);
        uint256 relatedValue = 0;

        for (uint256 i = 0; i < investors.length; i++) {

            relatedValue = projects.investorBalance(projectId, investors[i])
                .mul(value.mul(100).div(project.softCap))
                .div(100);

            token.mint(
                investors[i], 
                projects.serviceController.calculateTokens(
                    relatedValue, 
                    project.softCap, 
                    nativeFactor
                )
            );
        }
    }

    /**
     * @dev Transfer an asset
     * @param projectId Project Id
     * @param from Sender address
     * @param to Recipient address
     * @param value Amount of an asset
     */
    function transferAsset(
        bytes32 projectId,
        address from,
        address payable to,
        uint256 value
    ) internal returns (
        AssetsLib.AssetType assetType,
        address assetAddress,
        uint256 nativeFactor,
        bool removed
    ) {
        require(from != address(0), "WRONG_SENDER_ADDRESS");
        require(to != address(0), "WRONG_RECIPIENT_ADDRESS");
        require(value > 0, "NON_ZERO_VALUE_REQUIRED");

        ProjectsLib.Project storage project = projects.getProject(projectId);
        (assetType, assetAddress, nativeFactor, removed) = projects.serviceController.getAsset(project.assetId);

        uint256 fromBalance = from == address(this) ? address(this).balance : msg.value;

        if (assetType == AssetsLib.AssetType.Native) {
            require(fromBalance >= value, "INSUFFICIENT_BALANCE");

            if (from == address(this)) {                
                to.transfer(value);
            } else {

                // Accept only a specified value
                // the rest of value should be sent back
                if (msg.value > value) {
                    address(uint160(from)).transfer(msg.value.sub(value));
                }
            }            
        } else if (assetType == AssetsLib.AssetType.Erc20) {

            IERC20 erc20asset = IERC20(assetAddress);
            require(erc20asset.balanceOf(from) >= value, "INSUFFICIENT_BALANCE");

            if (from == address(this)) {
                erc20asset.safeTransfer(to, value);
            } else {
                require(erc20asset.allowance(from, to) >= value, "INSUFFICIENT_ALLOWANCE");
                erc20asset.safeTransferFrom(from, to, value);    
            }            
        } else {
            revert("UNKNOWN_ASSET_TYPE");// should never happen
        }
    }

    /**
     * @dev Make investment
     * @param projectId Project Id
     * @param from Delegated sender address
     * @param value Imvestment value
     */
    function doInvestment(
        bytes32 projectId, 
        address from,
        uint256 value
    ) internal {
        require(msg.sender != address(0), "WRONG_SENDER_ADDRESS");
        require(from != address(0), "WRONG_FROM_ADDRESS");
        require(value > 0, "NON_ZERO_VALUE_REQUIRED");        
        projects.registerInvestment(projectId, from, value);
        transferAsset(projectId, msg.sender, address(uint160(address(this))), value);
        emit Investment(projectId, from, value);
    }

    /**
     * @dev Change project state to the new one
     * @param projectId Project Id
     * @param newState New project state
     */
    function moveProjectToState(
        bytes32 projectId,
        ProjectsLib.State newState
    ) internal {
        ProjectsLib.State oldState = projects.moveToState(projectId, newState, time());
        emit ProjectStateChanged(projectId, newState, oldState);
    }
}
