pragma solidity 0.5.11;

import "../libraries/AssetsLib.sol";


/**
 * @title Assets registry interface
 * @dev This contract holds AssetsRegistry functions interface 
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 */
contract IAssetsRegistry {
    function isAsset(bytes32 assetId) external view returns (bool);
    function isRemoved(bytes32 assetId) external view returns (bool);
    function getAsset(bytes32 assetId) external view returns (
        AssetsLib.AssetType assetType,
        address assetAddress,
        uint256 nativeFactor,
        bool removed
    );
}