pragma solidity 0.5.11;

import "openzeppelin-solidity/contracts/introspection/ERC165.sol";
import "openzeppelin-solidity/contracts/introspection/ERC165Checker.sol";
import "openzeppelin-solidity/contracts/token/ERC20/IERC20.sol";
import "openzeppelin-solidity/contracts/token/ERC20/SafeERC20.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/lifecycle/Pausable.sol";
import "openzeppelin-solidity/contracts/utils/ReentrancyGuard.sol";
import "./IUnstoppable.sol";
import "./registries/AssetsRegistry.sol";
import "./libraries/AssetsLib.sol";


/**
 * @title Unstoppable Fundraising Platform (UFP)
 * @dev This contract holds main logic and storages of the UFP
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 */
contract Unstoppable is IUnstoppable, ERC165, Ownable, Pausable, ReentrancyGuard, AssetsRegistry {

    using SafeMath for uint256;
    using SafeERC20 for IERC20;

    /// @dev Version number
    string public constant version = "0.1.0";

    /// @dev Service fee rate
    uint256 internal fee = 500;// Fee rate multiplied by 10000: (0.05 * 10000 = 500)

    /**
     * @dev This event will be emitted when service fee has been changed
     * @param fee New service fee
     * @param oldFee Previous fee value
     * @param owner Contract owner
     */
    event FeeUpdated(
        uint256 fee,
        uint256 oldFee,
        address owner
    );

    /**
     * @dev This event will be emitted when owner did fee withdrawal
     * @param id Asset Id
     * @param owner Ivestors address
     * @param value Investment value
     */
    event WithdrawFee(
        bytes32 id,
        address owner,
        uint256 value
    );

    /**
     * @dev Unstoppable contract constructor
     */
    constructor() public {
        _registerInterface(getInterfaceId());
        emit FeeUpdated(500, 0, msg.sender);
    }

    /**
     * @dev Fallback function
     */
    function() external payable {
        // Direct payments to the contract are accepted
    }

    /**
     * @dev Get service fee
     * @return uint256
     */
    function serviceFee() external view returns (uint256) {
        return fee;
    }

    /**
     * @dev Set new service fee value
     * @param _fee New service fee
     */
    function setFee(uint256 _fee) external onlyOwner whenPaused {
        uint256 oldFee = fee;
        fee = _fee;
        emit FeeUpdated(fee, oldFee, msg.sender);
    }

    /**
     * @dev Calculate value of the tokens on the base of the investment
     * @param investmentValue Value of the investment
     * @param softCap Project soft cap
     * @param nativeFactor Asset native factor
     * @return uint256 Tokens value
     */
    function calculateTokens(
        uint256 investmentValue,
        uint256 softCap,
        uint256 nativeFactor
    ) external pure returns (uint256) {   
        uint256 tokensValue = investmentValue
            .mul(100)
            .mul(nativeFactor)
            .div(softCap);

        // For cases with extremely small investments
        if (tokensValue == 0) {
            tokensValue = 1;
        }

        return tokensValue;
    }

    /**
     * @dev Withdraw service fee
     * @param assetId Asset Id
     * @param value Amount of funds to withdraw
     */
    function withdraw(
        bytes32 assetId,
        uint256 value
    ) external onlyOwner nonReentrant {
        (AssetsLib.AssetType assetType, address assetAddress, , ) = assets.get(assetId);

        if (assetType == AssetsLib.AssetType.Native) {
            require(address(this).balance >= value, "INSUFICIENT_FUNDS");
            msg.sender.transfer(value);
        } else {
            IERC20 erc20Asset = IERC20(assetAddress);
            require(erc20Asset.balanceOf(address(this)) >= value, "INSUFICIENT_FUNDS");
            erc20Asset.safeTransfer(msg.sender, value);
        }

        emit WithdrawFee(assetId, msg.sender, value);
    }

    /**
     * @dev Calculating the Interface ID for the Unstoppable
     * @return bytes4 ERC165 Interface ID
     */
    function getInterfaceId() public pure returns (bytes4) {
        IUnstoppable i;
        // 0x3404764d
        return i.isAsset.selector ^
            i.getAsset.selector ^
            i.serviceFee.selector ^
            i.calculateTokens.selector ^ 
            i.paused.selector;
    }        
}