require('chai')
    .use(require('bn-chai')(web3.utils.BN))
    .should();

const { AssetsType, zeroAddress } = require('../helpers/constants');
const assertRevert = require('../helpers/assertRevert');

const AssetsRegistry = artifacts.require('AssetsRegistry');
const Erc20Token = artifacts.require('Erc20Token');

contract('AssetsRegistry', ([owner1]) => {
    let ar;
    let erc20token;

    beforeEach(async () => {
        ar = await AssetsRegistry.new({
            from: owner1
        });
        await ar.methods['pause()'].sendTransaction({
            from: owner1
        });
        erc20token = await Erc20Token.new('Test Coin', 'TEST', 18, web3.utils.toBN('10000000000000000000000000'), {
            from: owner1
        });
    });

    describe('Public methods', () => {

        describe('#getInterfaceId', () => {

            it('should return ERC165 interface ID', async () => {
                const id = await ar.methods['getInterfaceId()'].call();
                // console.log('!!!', id);
                (id).should.equal('0x365efc2a');
            });
        });

        describe('#addAsset(uint8,address,uint256)', () => {

            it('should add native asset', async () => {
                const result = await ar.methods['addAsset(uint8,address,uint256)'].sendTransaction(
                    AssetsType.Native, 
                    zeroAddress, 
                    web3.utils.toBN('1000000'),
                    {
                        from: owner1
                    });
                const events = result.logs.filter(l => l.event === 'AssetAdded');
                (events.length).should.equal(1);
                (events[0].args.id).should.be.a('string');
            });

            it('should fail with Native asset if passed non-zero address', async () => {
                await assertRevert(ar.methods['addAsset(uint8,address,uint256)'].sendTransaction(
                    AssetsType.Native, 
                    '0x0000000000000000000000000000000000000001',
                    web3.utils.toBN('1000000'), 
                    {
                        from: owner1
                    }), 'NATIVE_TYPE_REQUIRE_ZERO_ADDRESS');
            });

            it('should add erc20 asset', async () => {
                const result = await ar.methods['addAsset(uint8,address,uint256)'].sendTransaction(
                    AssetsType.Erc20, 
                    erc20token.address,
                    web3.utils.toBN('5521'), 
                    {
                        from: owner1
                    });
                const events = result.logs.filter(l => l.event === 'AssetAdded');
                (events.length).should.equal(1);
                (events[0].args.id).should.be.a('string');
            });

            it('should fail with Erc20 asset if passed zero address', async () => {
                await assertRevert(ar.methods['addAsset(uint8,address,uint256)'].sendTransaction(
                    AssetsType.Erc20, 
                    zeroAddress, 
                    web3.utils.toBN('5521'),
                    {
                        from: owner1
                    }), 'NON_ZERO_ADDRESS_REQUIRED');
            });

            it.skip('should fail if called by not an owner', async () => {});
        });

        describe('#updateAsset(bytes32,address,uint256)', () => {

            it('should update existed asset', async () => {
                const addResult = await ar.methods['addAsset(uint8,address,uint256)'].sendTransaction(
                    AssetsType.Erc20, 
                    erc20token.address, 
                    web3.utils.toBN('5521'),
                    {
                        from: owner1
                    });
                const addEvents = addResult.logs.filter(l => l.event === 'AssetAdded');
                const newErc20Token = await Erc20Token.new('Test Coin', 'TEST', 18, web3.utils.toBN('10000000000000000000000000'), {
                    from: owner1
                });

                const result = await ar.methods['updateAsset(bytes32,address,uint256)'].sendTransaction(
                    addEvents[0].args.id, 
                    newErc20Token.address, 
                    web3.utils.toBN('5520'),
                    {
                        from: owner1
                    });
                const events = result.logs.filter(l => l.event === 'AssetUpdated');
                (events.length).should.equal(1);
                (events[0].args.addr).should.equal(newErc20Token.address);
            });

            it('should fail if asset not exist', async () => {
                await assertRevert(ar.methods['updateAsset(bytes32,address,uint256)'].sendTransaction(
                    web3.utils.asciiToHex('test'), 
                    erc20token.address, 
                    web3.utils.toBN('1000000'),
                    {
                        from: owner1
                    }), 'ASSET_NOT_FOUND');
            });

            it.skip('should fail if called by not an owner', async () => {});
        });

        describe('#removeAsset(bytes32)', () => {

            it('should remove existed asset', async () => {
                const addResult = await ar.methods['addAsset(uint8,address,uint256)'].sendTransaction(
                    AssetsType.Erc20, 
                    erc20token.address, 
                    web3.utils.toBN('5521'),
                    {
                        from: owner1
                    });
                const addEvents = addResult.logs.filter(l => l.event === 'AssetAdded');

                const result = await ar.methods['removeAsset(bytes32)'].sendTransaction(
                    addEvents[0].args.id,
                    {
                        from: owner1
                    });
                const events = result.logs.filter(l => l.event === 'AssetRemoved');
                (events.length).should.equal(1);
                (events[0].args.id).should.equal(addEvents[0].args.id);
            });

            it('should fail if asset not exist', async () => {
                await assertRevert(ar.methods['removeAsset(bytes32)'].sendTransaction(
                    web3.utils.asciiToHex('test'),
                    {
                        from: owner1
                    }), 'ASSET_NOT_FOUND');
            });

            it.skip('should fail if called by not an owner', async () => {});
        });

        describe('#getAsset(bytes32)', () => {

            it('should return existed asset', async () => {
                const addResult = await ar.methods['addAsset(uint8,address,uint256)'].sendTransaction(
                    AssetsType.Erc20, 
                    erc20token.address, 
                    web3.utils.toBN('5521'),
                    {
                        from: owner1
                    });
                const addEvents = addResult.logs.filter(l => l.event === 'AssetAdded');
                const asset = await ar.methods['getAsset(bytes32)'].call(addEvents[0].args.id, {
                    from: owner1
                });
                (asset.assetType).should.eq.BN(AssetsType.Erc20);
                (asset.assetAddress).should.equal(erc20token.address);
            });

            it('should fail if asset not exist', async () => {
                await assertRevert(ar.methods['getAsset(bytes32)'].call(
                    web3.utils.asciiToHex('test'),
                    {
                        from: owner1
                    }), 'ASSET_NOT_FOUND');
            });
        });

        describe('#getAssets()', () => {

            it('should return list of Ids', async () => {
                const addResult = await ar.methods['addAsset(uint8,address,uint256)'].sendTransaction(
                    AssetsType.Erc20, 
                    erc20token.address, 
                    web3.utils.toBN('5521'),
                    {
                        from: owner1
                    });
                const addEvents = addResult.logs.filter(l => l.event === 'AssetAdded');
                const assets = await ar.methods['getAssets()'].call({
                    from: owner1
                });
                (assets.length).should.equal(1);
                (assets[0]).should.equal(addEvents[0].args.id);
            });

            it('should return list of Ids if some deleted', async () => {
                const tokens = [
                    '0xDC1323E899007bC051e73E5EaF24249385716cBB',
                    '0x210BaC5693D3C09CBd43B440964C8A4D790c8186',
                    '0xBAa4CbB7b9dd247200F267005614f976Cf975e60'
                ];
                await Promise.all(tokens.map(t => ar.methods['addAsset(uint8,address,uint256)'].sendTransaction(
                    AssetsType.Erc20, 
                    t, 
                    web3.utils.toBN('5521'),
                    {
                        from: owner1
                    })));
                const assetsOrig = await ar.methods['getAssets()'].call({
                    from: owner1
                });

                // remove one
                await ar.methods['removeAsset(bytes32)'].sendTransaction(
                    assetsOrig[1],
                    {
                        from: owner1
                    });
                const assets = await ar.methods['getAssets()'].call({
                    from: owner1
                });
                (assets.includes(assetsOrig[1])).should.be.false;

                // remove all assets
                await Promise.all(assets.map(id => ar.methods['removeAsset(bytes32)'].sendTransaction(
                    id, 
                    {
                        from: owner1
                    })));
                const empty = await ar.methods['getAssets()'].call({
                    from: owner1
                });
                (empty.length).should.equal(0);
            });
        });
    });
});
