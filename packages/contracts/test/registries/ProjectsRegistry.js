require('chai')
    .use(require('bn-chai')(web3.utils.BN))
    .should();

const { AssetsType, UrlType, zeroAddress, ProjectState } = require('../helpers/constants');
const { isHash } = require('../helpers/validators');
const {
    addProject, 
    updateProject,
    createProjectWithNativeAsset,
    createProjectWithErc20Asset,
    makeInvestment,
    doInvestments
} = require('../helpers/projects');
const { 
    assertInvestmentTokens, 
    calculateWithdraw,
    calculateVotingThreshold
} = require('../helpers/economics');
const { isqrt } = require('../helpers/bnmath');
const assertRevert = require('../helpers/assertRevert');
const UNFToken = artifacts.require('UNFToken');
const Unstoppable = artifacts.require('Unstoppable');
const ProjectsRegistry = artifacts.require('ProjectsRegistry');
const ProjectsRegistryTests = artifacts.require('ProjectsRegistryTests');
const ProjectRegistryReentracyTests = artifacts.require('ProjectRegistryReentracyTests');

contract('ProjectsRegistry', ([owner1, owner2, owner3, owner4, owner5]) => {
    let token;
    let ar;
    let pr;
    let prTime;
    let assetId;
    let feeRate;

    beforeEach(async () => {
        token = await UNFToken.new(owner1, web3.utils.toBN(web3.utils.toWei('1000000', 'ether')), {
            from: owner1
        });
        ar = await Unstoppable.new({
            from: owner1
        });
        feeRate = await ar.methods['serviceFee()'].call();
        pr = await ProjectsRegistry.new(ar.address, token.address, {
            from: owner1
        });
        prTime = await ProjectsRegistryTests.new(ar.address, token.address, {
            from: owner1
        });
        await token.methods['addWhitelisted(address)'].sendTransaction(pr.address, {
            from: owner1
        });
        await token.methods['addWhitelisted(address)'].sendTransaction(prTime.address, {
            from: owner1
        });
        await ar.methods['pause()'].sendTransaction({
            from: owner1
        });
        const result = await ar.methods['addAsset(uint8,address,uint256)'].sendTransaction(
            AssetsType.Native, 
            zeroAddress, 
            web3.utils.toBN('1000000'),
            {
                from: owner1
            });
        const events = result.logs.filter(l => l.event === 'AssetAdded');
        assetId = events[0].args.id;
        await ar.methods['unpause()'].sendTransaction({
            from: owner1
        });
    });

    describe('Constructor', () => {

        it('should fail if wrong Unstoppable address provided', async () => {
            await assertRevert(ProjectsRegistry.new(zeroAddress, token.address, {
                from: owner1
            }), 'NOT_SUPPORTED_CONTROLLER_INTERFACE');
        });

        it('should fail if wrong UNFToken address provided', async () => {
            await assertRevert(ProjectsRegistry.new(ar.address, zeroAddress, {
                from: owner1
            }), 'NOT_SUPPORTED_TOKEN_INTERFACE');
        });
    });

    describe('Methods for the project owner', () => {

        describe('#addProject', () => {
            
            it('should add new project to the registry', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('30000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('50000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('200'),
                        budget: web3.utils.toBN('60000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*2);//now + 2 days
                const stopDate = startDate + (60*60*24*60);

                const result = await addProject(
                    pr,
                    owner1,
                    'querty',
                    UrlType.Ipfs,
                    assetId,
                    '170000000000000000000',
                    startDate,
                    stopDate,
                    milestones
                );
                
                const events = result.logs.filter(l => l.event === 'ProjectAdded');
                (isHash(events[0].args.id)).should.be.true;
                (web3.utils.isAddress(events[0].args.owner)).should.be.true;
            });

            it('should fail if project description not provided', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('30000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('50000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('200'),
                        budget: web3.utils.toBN('60000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*2);//now + 2 days
                const stopDate = startDate + (60*60*24*60);

                await assertRevert(addProject(
                    pr,
                    owner1,
                    '',
                    UrlType.Ipfs,
                    assetId,
                    '170000000000000000000',
                    startDate,
                    stopDate,
                    milestones
                ), 'PROJECT_DECRIPTION_REQUIRED');
            });

            it('should fail if provided hardCap value too low', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('30000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('50000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('200'),
                        budget: web3.utils.toBN('60000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*2);//now + 2 days
                const stopDate = startDate + (60*60*24*60);

                await assertRevert(addProject(
                    pr,
                    owner1,
                    'querty',
                    UrlType.Ipfs,
                    assetId,
                    '140000000000000000000',
                    startDate,
                    stopDate,
                    milestones
                ), 'HARDCAP_TOO_LOW');
            });

            it('should fail if provided hardCap value too high', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('30000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('50000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('200'),
                        budget: web3.utils.toBN('60000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*2);//now + 2 days
                const stopDate = startDate + (60*60*24*60);

                await assertRevert(addProject(
                    pr,
                    owner1,
                    'querty',
                    UrlType.Ipfs,
                    assetId,
                    '300000000000000000000',
                    startDate,
                    stopDate,
                    milestones
                ), 'HARDCAP_TOO_HIGH');
            });

            it('should fail if provided stopDate earlier then startDate', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('30000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('50000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('200'),
                        budget: web3.utils.toBN('60000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*2);//now + 2 days
                const stopDate = startDate + (60*60*24*60);

                await assertRevert(addProject(
                    pr,
                    owner1,
                    'querty',
                    UrlType.Ipfs,
                    assetId,
                    '170000000000000000000',
                    stopDate,
                    startDate,
                    milestones
                ), 'STOPDATE_EARLIER_THEN_STARTDATE');
            });

            it('should fail if fundraising period too short', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('30000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('50000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('200'),
                        budget: web3.utils.toBN('60000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*2);
                const stopDate = startDate + (60*60*24*0.5);

                await assertRevert(addProject(
                    pr,
                    owner1,
                    'querty',
                    UrlType.Ipfs,
                    assetId,
                    '170000000000000000000',
                    startDate,
                    stopDate,
                    milestones
                ), 'FUNDRAISING_PERIOD_TOO_SHORT');
            });

            it('should fail if milestones configuration properties not consistent', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('30000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('50000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('200'),
                        budget: web3.utils.toBN('60000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*0.5);//now + 0.5 days
                const stopDate = startDate + (60*60*24*60);

                await assertRevert(addProject(
                    pr,
                    owner1,
                    'querty',
                    UrlType.Ipfs,
                    assetId,
                    '170000000000000000000',
                    startDate,
                    stopDate,
                    milestones,
                    'SIMULATE_INCONSISTANCE'
                ), 'MILESTONES_CONFIG_NOT_CONSISTENT');
            });

            it('should fail if milestones count not fulfill required value', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('30000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*0.5);//now + 0.5 days
                const stopDate = startDate + (60*60*24*60);

                await assertRevert(addProject(
                    pr,
                    owner1,
                    'querty',
                    UrlType.Ipfs,
                    assetId,
                    '170000000000000000000',
                    startDate,
                    stopDate,
                    milestones
                ), 'WRONG_MILESTONES_COUNT');
            });

            it('should fail if first milestone budget too high', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('70000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('50000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('200'),
                        budget: web3.utils.toBN('60000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*0.5);//now + 0.5 days
                const stopDate = startDate + (60*60*24*60);

                await assertRevert(addProject(
                    pr,
                    owner1,
                    'querty',
                    UrlType.Ipfs,
                    assetId,
                    '200000000000000000000',
                    startDate,
                    stopDate,
                    milestones
                ), 'FIRST_MILESTONE_BUDGET_TOO_HIGH');
            });
        });

        describe('#updateProject', () => {
            let projectId;
            let assetId2;

            beforeEach(async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('30000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('50000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('200'),
                        budget: web3.utils.toBN('60000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*2);//now + 2 days
                const stopDate = startDate + (60*60*24*60);

                const result = await addProject(
                    pr,
                    owner1,
                    'querty',
                    UrlType.Ipfs,
                    assetId,
                    '170000000000000000000',
                    startDate,
                    stopDate,
                    milestones
                );
                
                const events = result.logs.filter(l => l.event === 'ProjectAdded');
                projectId = events[0].args.id;

                const assetsResult = await ar.methods['addAsset(uint8,address,uint256)'].sendTransaction(
                    AssetsType.Erc20, 
                    '0x4F209f92F5cDE44FFF98C0b0CA486B14424e2834', 
                    web3.utils.toBN('5521'),
                    {
                        from: owner1
                    });
                const assetsEvents = assetsResult.logs.filter(l => l.event === 'AssetAdded');
                assetId2 = assetsEvents[0].args.id;
            });

            it('should update existed project', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('60000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('90000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*5);
                const stopDate = startDate + (60*60*24*120);

                const result = await updateProject(
                    pr,
                    projectId,
                    owner1,
                    'querty2',
                    UrlType.Swarm,
                    assetId2,
                    '190000000000000000000',
                    startDate,
                    stopDate,
                    milestones
                );
                
                const events = result.logs.filter(l => l.event === 'ProjectUpdated');
                (isHash(events[0].args.id)).should.be.true;
                (events[0].args.id).should.equal(projectId);
                (web3.utils.isAddress(events[0].args.owner)).should.be.true;                
            });

            it('should fail if wrong project Id provided', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('60000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('90000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*5);
                const stopDate = startDate + (60*60*24*120);

                await assertRevert(updateProject(
                    pr,
                    web3.utils.asciiToHex('test'),
                    owner1,
                    'querty2',
                    UrlType.Swarm,
                    assetId2,
                    '190000000000000000000',
                    startDate,
                    stopDate,
                    milestones
                ), 'PROJECT_NOT_FUND');
            });

            it('should fail if called by not a project owner', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('60000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('90000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*5);
                const stopDate = startDate + (60*60*24*120);

                await assertRevert(updateProject(
                    pr,
                    projectId,
                    owner2,
                    'querty2',
                    UrlType.Swarm,
                    assetId2,
                    '190000000000000000000',
                    startDate,
                    stopDate,
                    milestones
                ), 'NOT_AN_OWNER');
            });

            it('should fail if project description not provided', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('60000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('90000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*5);
                const stopDate = startDate + (60*60*24*120);

                await assertRevert(updateProject(
                    pr,
                    projectId,
                    owner1,
                    '',
                    UrlType.Swarm,
                    assetId2,
                    '190000000000000000000',
                    startDate,
                    stopDate,
                    milestones
                ), 'PROJECT_DECRIPTION_REQUIRED');
            });

            it('should fail if provided hardCap value too small', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('60000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('90000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*5);
                const stopDate = startDate + (60*60*24*120);

                await assertRevert(updateProject(
                    pr,
                    projectId,
                    owner1,
                    'querty2',
                    UrlType.Swarm,
                    assetId2,
                    '140000000000000000000',
                    startDate,
                    stopDate,
                    milestones
                ), 'HARDCAP_TOO_LOW');
            });

            it('should fail if provided hardCap value too high', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('60000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('90000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*5);
                const stopDate = startDate + (60*60*24*120);

                await assertRevert(updateProject(
                    pr,
                    projectId,
                    owner1,
                    'querty2',
                    UrlType.Swarm,
                    assetId2,
                    '340000000000000000000',
                    startDate,
                    stopDate,
                    milestones
                ), 'HARDCAP_TOO_HIGH');
            });

            it('should fail if provided stopDate earlier then startDate', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('60000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('90000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*5);
                const stopDate = startDate + (60*60*24*120);

                await assertRevert(updateProject(
                    pr,
                    projectId,
                    owner1,
                    'querty2',
                    UrlType.Swarm,
                    assetId2,
                    '190000000000000000000',
                    stopDate,
                    startDate,
                    milestones
                ), 'STOPDATE_EARLIER_THEN_STARTDATE');
            });

            it('should fail if fundraising period too short', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('60000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('90000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*5);
                const stopDate = startDate + (60*60*24*0.5);

                await assertRevert(updateProject(
                    pr,
                    projectId,
                    owner1,
                    'querty2',
                    UrlType.Swarm,
                    assetId2,
                    '190000000000000000000',
                    startDate,
                    stopDate,
                    milestones
                ), 'FUNDRAISING_PERIOD_TOO_SHORT');
            });

            it('should fail if milestones configuration properties not consistent', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('60000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('90000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*5);
                const stopDate = startDate + (60*60*24*120);

                await assertRevert(updateProject(
                    pr,
                    projectId,
                    owner1,
                    'querty2',
                    UrlType.Swarm,
                    assetId2,
                    '190000000000000000000',
                    startDate,
                    stopDate,
                    milestones,
                    'SIMULATE_INCONSISTANCE'
                ), 'MILESTONES_CONFIG_NOT_CONSISTENT');
            });

            it('should fail if milestones count not fulfill required value', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('60000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*5);
                const stopDate = startDate + (60*60*24*120);

                await assertRevert(updateProject(
                    pr,
                    projectId,
                    owner1,
                    'querty2',
                    UrlType.Swarm,
                    assetId2,
                    '190000000000000000000',
                    startDate,
                    stopDate,
                    milestones
                ), 'WRONG_MILESTONES_COUNT');
            });

            it('should fail if first milestone budget too high', async () => {
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('120000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('20000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*5);
                const stopDate = startDate + (60*60*24*120);

                await assertRevert(updateProject(
                    pr,
                    projectId,
                    owner1,
                    'querty2',
                    UrlType.Swarm,
                    assetId2,
                    '160000000000000000000',
                    startDate,
                    stopDate,
                    milestones
                ), 'FIRST_MILESTONE_BUDGET_TOO_HIGH');
            });

            it('should fail if project already closed', async () => {
                await pr.methods['closeProject(bytes32)'].sendTransaction(
                    projectId,
                    {
                        from: owner1
                    }
                );
                const milestones = [
                    {
                        duration: web3.utils.toBN('100'),
                        budget: web3.utils.toBN('50000000000000000000')
                    },
                    {
                        duration: web3.utils.toBN('150'),
                        budget: web3.utils.toBN('9000000000000000000')
                    }
                ];
                const startDate = Math.round(Date.now()/1000)+(60*60*24*5);
                const stopDate = startDate + (60*60*24*120);

                await assertRevert(updateProject(
                    pr,
                    projectId,
                    owner1,
                    'querty2',
                    UrlType.Swarm,
                    assetId2,
                    '160000000000000000000',
                    startDate,
                    stopDate,
                    milestones
                ), 'NOT_ALLOWED_IN_CURRENT_STATE');
            });
        });

        describe('#closeProject', () => {
            let projectNative;

            beforeEach(async () => {
                projectNative = await createProjectWithNativeAsset(pr, ar, owner1, null, null, null, true);// with not started campaign
            });

            it('should close existed project', async () => {
                const result = await pr.methods['closeProject(bytes32)'].sendTransaction(
                    projectNative.projectId,
                    {
                        from: owner1
                    }
                );

                const events = result.logs.filter(l => l.event === 'ProjectStateChanged');
                (isHash(events[0].args.id)).should.be.true;
                (events[0].args.id).should.equal(projectNative.projectId); 
                (events[0].args.newState.toNumber()).should.equal(ProjectState.Closed);
            });

            it('should fail if wrong project Id provided', async () => {
                await assertRevert(pr.methods['closeProject(bytes32)'].sendTransaction(
                    web3.utils.asciiToHex('test'),
                    {
                        from: owner1
                    }
                ), 'PROJECT_NOT_FUND');
            });

            it('should fail if called by not a project owner', async () => {
                await assertRevert(pr.methods['closeProject(bytes32)'].sendTransaction(
                    projectNative.projectId,
                    {
                        from: owner2
                    }
                ), 'NOT_AN_OWNER');
            });
        });

        describe('#startCampaign', () => {
            let projectNative;

            beforeEach(async () => {
                projectNative = await createProjectWithNativeAsset(pr, ar, owner1, null, null, null, true);// with not started campaign
            });

            it('should start project funding campaign', async () => {
                const result = await pr.methods['startCampaign(bytes32)'].sendTransaction(
                    projectNative.projectId,
                    {
                        from: owner1
                    }
                );

                const events = result.logs.filter(l => l.event === 'ProjectStateChanged');
                (isHash(events[0].args.id)).should.be.true;
                (events[0].args.id).should.equal(projectNative.projectId);
                (events[0].args.newState.toNumber()).should.equal(ProjectState.Campaign);
            });

            it('should fail if wrong project Id provided', async () => {
                await assertRevert(pr.methods['startCampaign(bytes32)'].sendTransaction(
                    web3.utils.asciiToHex('test'),
                    {
                        from: owner1
                    }
                ), 'PROJECT_NOT_FUND');
            });

            it('should fail if called by not a project owner', async () => {
                await assertRevert(pr.methods['startCampaign(bytes32)'].sendTransaction(
                    projectNative.projectId,
                    {
                        from: owner2
                    }
                ), 'NOT_AN_OWNER');
            });

            it('should fail if project already closed', async () => {
                await pr.methods['closeProject(bytes32)'].sendTransaction(
                    projectNative.projectId,
                    {
                        from: owner1
                    }
                );
                await assertRevert(pr.methods['startCampaign(bytes32)'].sendTransaction(
                    projectNative.projectId,
                    {
                        from: owner1
                    }
                ), 'TRANSITION_NOT_ALLOWED');
            });
        });

        describe('#startProject', () => {
            let projectNative;
            let projectErc20;
            let projectNativeNoCampaign;
            let projectErc20NoCampaign;
            let projectNativeNtl;
            let projectErc20Ntl;
            let startDate;
            let stopDate;

            beforeEach(async () => {
                projectNative = await createProjectWithNativeAsset(pr, ar, owner1);
                projectErc20 = await createProjectWithErc20Asset(pr, ar, owner1, [owner3, owner4, owner5]);
                projectNativeNoCampaign = await createProjectWithNativeAsset(pr, ar, owner1, null, null, null, true);
                projectErc20NoCampaign = await createProjectWithErc20Asset(pr, ar, owner1, [owner3, owner4, owner5], null, null, null, true);
                projectNativeNtl = await createProjectWithNativeAsset(prTime, ar, owner1, null, startDate, stopDate);
                projectErc20Ntl = await createProjectWithErc20Asset(prTime, ar, owner1, [owner3, owner4, owner5], null, startDate, stopDate);
                startDate = Math.round(Date.now()/1000) + 2*24*60*60;//+2 days
                stopDate = startDate + 10*24*60*60;
            });

            it('should start project', async () => {
                await makeInvestment(pr, ar, projectNative.projectId, '10', owner3);
                await makeInvestment(pr, ar, projectNative.projectId, '100', owner4);
                await makeInvestment(pr, ar, projectNative.projectId, '50', owner5);
                await makeInvestment(pr, ar, projectErc20.projectId, '10', owner3);
                await makeInvestment(pr, ar, projectErc20.projectId, '100', owner4);
                await makeInvestment(pr, ar, projectErc20.projectId, '50', owner5);

                const result1 = await pr.methods['startProject(bytes32)'].sendTransaction(projectNative.projectId, {
                    from: owner1
                });
                const events1 = result1.logs.filter(l => l.event === 'ProjectStateChanged');
                (events1.length).should.equal(1);
                (events1[0].args.id).should.equal(projectNative.projectId);
                (events1[0].args.newState.toNumber()).should.equal(ProjectState.Started);
                ((await pr.methods['projectState(bytes32)'].call(projectNative.projectId)).toNumber()).should.equal(ProjectState.Started);

                const result2 = await pr.methods['startProject(bytes32)'].sendTransaction(projectErc20.projectId, {
                    from: owner1
                });
                const events2 = result2.logs.filter(l => l.event === 'ProjectStateChanged');
                (events2.length).should.equal(1);
                (events2[0].args.id).should.equal(projectErc20.projectId);
                (events2[0].args.newState.toNumber()).should.equal(ProjectState.Started);
                ((await pr.methods['projectState(bytes32)'].call(projectErc20.projectId)).toNumber()).should.equal(ProjectState.Started);
            });

            it('should move the project to the CampaignFailed state if softCap not reached after campaign ended', async () => {
                await makeInvestment(prTime, ar, projectNativeNtl.projectId, '10', owner3);
                await makeInvestment(prTime, ar, projectNativeNtl.projectId, '50', owner4);
                await makeInvestment(prTime, ar, projectErc20Ntl.projectId, '10', owner3);
                await makeInvestment(prTime, ar, projectErc20Ntl.projectId, '50', owner4);

                // set project time to the project startDate + 10sec
                await prTime.methods['setCurrentTime(uint256)'].sendTransaction(
                    web3.utils.toBN(startDate + 10),
                    {
                        from: owner1
                    }
                );

                const result1 = await prTime.methods['startProject(bytes32)'].sendTransaction(projectNativeNtl.projectId, {
                    from: owner1
                });
                const events1 = result1.logs.filter(l => l.event === 'ProjectStateChanged');
                (events1.length).should.equal(1);
                (events1[0].args.id).should.equal(projectNativeNtl.projectId);
                (events1[0].args.newState.toNumber()).should.equal(ProjectState.CampaignFailed);

                const result2 = await prTime.methods['startProject(bytes32)'].sendTransaction(projectErc20Ntl.projectId, {
                    from: owner1
                });
                const events2 = result2.logs.filter(l => l.event === 'ProjectStateChanged');
                (events2.length).should.equal(1);
                (events2[0].args.id).should.equal(projectErc20Ntl.projectId);
                (events2[0].args.newState.toNumber()).should.equal(ProjectState.CampaignFailed);
            });

            it('should fail if campaign not finished but sofCap not reached', async () => {
                await makeInvestment(prTime, ar, projectNativeNtl.projectId, '10', owner3);
                await makeInvestment(prTime, ar, projectNativeNtl.projectId, '50', owner4);
                await makeInvestment(prTime, ar, projectErc20Ntl.projectId, '10', owner3);
                await makeInvestment(prTime, ar, projectErc20Ntl.projectId, '50', owner4);

                // set project time to the project startDate - 10sec
                await prTime.methods['setCurrentTime(uint256)'].sendTransaction(
                    web3.utils.toBN(startDate - 10),
                    {
                        from: owner1
                    }
                );

                await assertRevert(prTime.methods['startProject(bytes32)'].sendTransaction(projectNativeNtl.projectId, {
                    from: owner1
                }), 'SOFTCAP_NOT_REACHED');
                await assertRevert(prTime.methods['startProject(bytes32)'].sendTransaction(projectErc20Ntl.projectId, {
                    from: owner1
                }), 'SOFTCAP_NOT_REACHED');
            });

            it('should fail if project expired', async () => {
                await makeInvestment(prTime, ar, projectNativeNtl.projectId, '10', owner3);
                await makeInvestment(prTime, ar, projectNativeNtl.projectId, '50', owner4);
                await makeInvestment(prTime, ar, projectErc20Ntl.projectId, '10', owner3);
                await makeInvestment(prTime, ar, projectErc20Ntl.projectId, '50', owner4);

                // set project time to the project stopDate + 10sec
                await prTime.methods['setCurrentTime(uint256)'].sendTransaction(
                    web3.utils.toBN(stopDate + 10),
                    {
                        from: owner1
                    }
                );

                await assertRevert(prTime.methods['startProject(bytes32)'].sendTransaction(projectNativeNtl.projectId, {
                    from: owner1
                }), 'PROJECT_EXPIRED');
                await assertRevert(prTime.methods['startProject(bytes32)'].sendTransaction(projectErc20Ntl.projectId, {
                    from: owner1
                }), 'PROJECT_EXPIRED');
            });

            it('should fail if called by not a project owner', async () => {
                await assertRevert(pr.methods['startProject(bytes32)'].sendTransaction(projectNative.projectId, {
                    from: owner2
                }), 'NOT_AN_OWNER');
                await assertRevert(pr.methods['startProject(bytes32)'].sendTransaction(projectErc20.projectId, {
                    from: owner2
                }), 'NOT_AN_OWNER');
            });

            it('should fail if project not in Campaign state', async () => {
                await assertRevert(pr.methods['startProject(bytes32)'].sendTransaction(projectNativeNoCampaign.projectId, {
                    from: owner1
                }), 'NOT_ALLOWED_IN_CURRENT_STATE');
                await assertRevert(pr.methods['startProject(bytes32)'].sendTransaction(projectErc20NoCampaign.projectId, {
                    from: owner1
                }), 'NOT_ALLOWED_IN_CURRENT_STATE');
            });

            it('should fail if project already started', async () => {
                await makeInvestment(pr, ar, projectNative.projectId, '10', owner3);
                await makeInvestment(pr, ar, projectNative.projectId, '100', owner4);
                await makeInvestment(pr, ar, projectNative.projectId, '50', owner5);
                await makeInvestment(pr, ar, projectErc20.projectId, '10', owner3);
                await makeInvestment(pr, ar, projectErc20.projectId, '100', owner4);
                await makeInvestment(pr, ar, projectErc20.projectId, '50', owner5);

                await pr.methods['startProject(bytes32)'].sendTransaction(projectNative.projectId, {
                    from: owner1
                });
                await pr.methods['startProject(bytes32)'].sendTransaction(projectErc20.projectId, {
                    from: owner1
                });

                await assertRevert(pr.methods['startProject(bytes32)'].sendTransaction(projectNativeNoCampaign.projectId, {
                    from: owner1
                }), 'NOT_ALLOWED_IN_CURRENT_STATE');
                await assertRevert(pr.methods['startProject(bytes32)'].sendTransaction(projectErc20NoCampaign.projectId, {
                    from: owner1
                }), 'NOT_ALLOWED_IN_CURRENT_STATE');
            });
        });

        describe('Withdrawals', () => {
            let projectNative;
            let projectErc20;

            const milestones = [
                {
                    duration: web3.utils.toBN('100'),
                    budget: web3.utils.toBN(web3.utils.toWei('30', 'ether'))
                },
                {
                    duration: web3.utils.toBN('150'),
                    budget: web3.utils.toBN(web3.utils.toWei('50', 'ether'))
                },
                {
                    duration: web3.utils.toBN('200'),
                    budget: web3.utils.toBN(web3.utils.toWei('60', 'ether'))
                }
            ];

            const investments = [
                {
                    investor: owner3,
                    value: '10'
                },
                {
                    investor: owner4,
                    value: '100'
                },
                {
                    investor: owner5,
                    value: '50'
                }
            ];

            beforeEach(async () => {
                projectNative = await createProjectWithNativeAsset(pr, ar, owner1, milestones);
                projectErc20 = await createProjectWithErc20Asset(pr, ar, owner1, [owner3, owner4, owner5], milestones);
            });

            describe('#calculateWithdraw', () => {       
                    
                it('should return withdrawal limit', async () => {
                    await doInvestments(pr, ar, projectNative.projectId, owner1, investments, true);
                    const projectBalance = await pr.methods['projectBalance(bytes32)'].call(projectNative.projectId);
                    
                    const calculated = calculateWithdraw(
                        projectBalance,
                        '0',
                        milestones,
                        '0',
                        feeRate
                    );
                    const withdraw = await pr.methods['calculateWithdraw(bytes32)'].call(projectNative.projectId);
                    
                    (withdraw.value).should.eq.BN(calculated.value);
                    (withdraw.fee).should.eq.BN(calculated.fee);
                });
            });
    
            describe('#withdraw', () => {
                
                it('should withdraw milestone native funds', async () => {
                    await doInvestments(pr, ar, projectNative.projectId, owner1, investments, true);
                    const investorTokensBalance1 = await token.methods['balanceOf(address)'].call(owner3);
                    const investorTokensBalance2 = await token.methods['balanceOf(address)'].call(owner4);
                    const investorTokensBalance3 = await token.methods['balanceOf(address)'].call(owner5);
                    const ownBalance = await web3.eth.getBalance(owner1);
                    const registryBalance = await web3.eth.getBalance(pr.address);               
                    const projectBalance = await pr.methods['projectBalance(bytes32)'].call(projectNative.projectId);
                    const project = await pr.methods['getProject(bytes32)'].call(projectNative.projectId);
                    const shouldWithdraw = calculateWithdraw(
                        projectBalance,
                        '0',
                        milestones,
                        project.milestone,
                        feeRate
                    );
                    const result = await pr.methods['withdraw(bytes32)'].sendTransaction(
                        projectNative.projectId,
                        {
                            from: owner1
                        }
                    );
                    const events = result.logs.filter(l => l.event === 'Withdraw');
                    (events.length).should.equal(1);
                    (events[0].args.id).should.equal(projectNative.projectId);
                    (events[0].args.owner).should.equal(owner1);
                    (events[0].args.value).should.eq.BN(shouldWithdraw.value);
                    (events[0].args.fee).should.eq.BN(shouldWithdraw.fee);

                    const tx = await web3.eth.getTransactionFromBlock('pending', result.receipt.transactionIndex);
                    const gasPrice = web3.utils.toBN(web3.utils.toBN(tx.gasPrice));

                    (await pr.methods['projectBalance(bytes32)'].call(projectNative.projectId))
                        .should.eq.BN(projectBalance.sub(shouldWithdraw.value));
                    (await web3.eth.getBalance(owner1)).should.eq.BN(
                        web3.utils.toBN(ownBalance)
                            .add(shouldWithdraw.value)
                            .sub(web3.utils.toBN(result.receipt.gasUsed).mul(gasPrice))
                    );
                    (await web3.eth.getBalance(pr.address))
                        .should.eq.BN(web3.utils.toBN(registryBalance).sub(shouldWithdraw.value).sub(shouldWithdraw.fee));
                    await assertInvestmentTokens(pr, ar, token, projectNative.projectId, shouldWithdraw.value, owner3, investorTokensBalance1);
                    await assertInvestmentTokens(pr, ar, token, projectNative.projectId, shouldWithdraw.value, owner4, investorTokensBalance2);
                    await assertInvestmentTokens(pr, ar, token, projectNative.projectId, shouldWithdraw.value, owner5, investorTokensBalance3);
                });

                it('should withdraw milestone Erc20 funds', async () => {
                    await doInvestments(pr, ar, projectErc20.projectId, owner1, investments, true);
                    const investorTokensBalance1 = await token.methods['balanceOf(address)'].call(owner3);
                    const investorTokensBalance2 = await token.methods['balanceOf(address)'].call(owner4);
                    const investorTokensBalance3 = await token.methods['balanceOf(address)'].call(owner5);
                    const ownBalance = await projectErc20.token.methods['balanceOf(address)'].call(owner1);
                    const registryBalance = await projectErc20.token.methods['balanceOf(address)'].call(pr.address);
                    const projectBalance = await pr.methods['projectBalance(bytes32)'].call(projectErc20.projectId);
                    const project = await pr.methods['getProject(bytes32)'].call(projectErc20.projectId);
                    const shouldWithdraw = calculateWithdraw(
                        projectBalance,
                        '0',
                        milestones,
                        project.milestone,
                        feeRate
                    );
                    const result = await pr.methods['withdraw(bytes32)'].sendTransaction(
                        projectErc20.projectId,
                        {
                            from: owner1
                        }
                    );
                    const events = result.logs.filter(l => l.event === 'Withdraw');
                    (events.length).should.equal(1);
                    (events[0].args.id).should.equal(projectErc20.projectId);
                    (events[0].args.owner).should.equal(owner1);
                    (events[0].args.value).should.eq.BN(shouldWithdraw.value);
                    (events[0].args.fee).should.eq.BN(shouldWithdraw.fee);

                    (await pr.methods['projectBalance(bytes32)'].call(projectErc20.projectId))
                        .should.eq.BN(projectBalance.sub(shouldWithdraw.value));
                    (await projectErc20.token.methods['balanceOf(address)'].call(owner1))
                        .should.eq.BN(web3.utils.toBN(ownBalance).add(shouldWithdraw.value));
                    (await projectErc20.token.methods['balanceOf(address)'].call(pr.address))
                        .should.eq.BN(web3.utils.toBN(registryBalance).sub(shouldWithdraw.value).sub(shouldWithdraw.fee));
                    await assertInvestmentTokens(pr, ar, token, projectErc20.projectId, shouldWithdraw.value, owner3, investorTokensBalance1);
                    await assertInvestmentTokens(pr, ar, token, projectErc20.projectId, shouldWithdraw.value, owner4, investorTokensBalance2);
                    await assertInvestmentTokens(pr, ar, token, projectErc20.projectId, shouldWithdraw.value, owner5, investorTokensBalance3);
                });
            });
        });

        describe('#publishReport', () => {
            let projectNative;
            let projectErc20;

            const investments = [
                {
                    investor: owner3,
                    value: '10'
                },
                {
                    investor: owner4,
                    value: '100'
                },
                {
                    investor: owner5,
                    value: '50'
                }
            ];

            beforeEach(async () => {
                projectNative = await createProjectWithNativeAsset(pr, ar, owner1);
                projectErc20 = await createProjectWithErc20Asset(pr, ar, owner1, [owner3, owner4, owner5]);
            });

            it('should publish report and project (native asset) to the Voting state', async () => {
                await doInvestments(pr, ar, projectNative.projectId, owner1, investments, true);

                const result = await pr.methods['publishReport(bytes32,bytes32,uint8)'].sendTransaction(
                    projectNative.projectId,
                    web3.utils.asciiToHex('querty'),
                    UrlType.Ipfs,
                    {
                        from: owner1
                    }
                );
                const events = result.logs.filter(l => l.event === 'ReportPublished');
                (events.length).should.equal(1);
                (events[0].args.id).should.equal(projectNative.projectId);
                (events[0].args.reportId).should.be.string;
                (events[0].args.owner).should.equal(owner1);
                ((await pr.methods['projectState(bytes32)'].call(projectNative.projectId)).toNumber()).should.equal(ProjectState.Voting);
            });

            it('should publish report and project (Erc20 asset) to the Voting state', async () => {
                await doInvestments(pr, ar, projectErc20.projectId, owner1, investments, true);

                const result = await pr.methods['publishReport(bytes32,bytes32,uint8)'].sendTransaction(
                    projectErc20.projectId,
                    web3.utils.asciiToHex('querty'),
                    UrlType.Ipfs,
                    {
                        from: owner1
                    }
                );
                const events = result.logs.filter(l => l.event === 'ReportPublished');
                (events.length).should.equal(1);
                (events[0].args.id).should.equal(projectErc20.projectId);
                (events[0].args.reportId).should.be.string;
                (events[0].args.owner).should.equal(owner1);
                ((await pr.methods['projectState(bytes32)'].call(projectErc20.projectId)).toNumber()).should.equal(ProjectState.Voting);
            });
        });
    });

    describe('Common methods', () => {
        
        describe('#projectBalance', () => {
            let projectNative;
            let projectErc20;

            beforeEach(async () => {
                projectNative = await createProjectWithNativeAsset(pr, ar, owner1);
                projectErc20 = await createProjectWithErc20Asset(pr, ar, owner1, [owner3]);
            });

            it('should return balance of funds invested', async () => {
                const value = web3.utils.toBN(web3.utils.toWei('10', 'ether'));
                await pr.methods['invest(bytes32,uint256)'].sendTransaction(
                    projectNative.projectId,
                    value,
                    {
                        from: owner3,
                        value
                    }
                );
                (await pr.methods['projectBalance(bytes32)'].call(projectNative.projectId)).should.eq.BN(value);

                await projectErc20.token.methods['approve(address,uint256)'].sendTransaction(pr.address, value, { from: owner3 });                
                await pr.methods['invest(bytes32,uint256)'].sendTransaction(
                    projectErc20.projectId,
                    value,
                    { from: owner3 }
                );
                (await pr.methods['projectBalance(bytes32)'].call(projectErc20.projectId)).should.eq.BN(value);
            });

            it('should fail if unknown project Id has been provided', async () => {
                await assertRevert(pr.methods['projectBalance(bytes32)'].call(web3.utils.asciiToHex('unknownId')), 'PROJECT_NOT_FUND');
            });
        });
    });

    describe('Methods for the investor', () => {

        describe('#invest', () => {
            let projectNative;
            let projectErc20;
            let startDate;
            let stopDate;

            beforeEach(async () => {
                projectNative = await createProjectWithNativeAsset(pr, ar, owner1);
                projectErc20 = await createProjectWithErc20Asset(pr, ar, owner1, [owner3]);
                startDate = Math.round(Date.now()/1000) + 2*24*60*60;//+2 days
                stopDate = startDate + 10*24*60*60;
            });

            it('should make investment using native and erc20 assets', async () => {
                const registryBalance1 = await web3.eth.getBalance(pr.address);
                const value = web3.utils.toBN(web3.utils.toWei('10', 'ether'));
                const result1 = await pr.methods['invest(bytes32,uint256)'].sendTransaction(
                    projectNative.projectId,
                    value,
                    {
                        from: owner3,
                        value
                    }
                );

                const events1 = result1.logs.filter(l => l.event === 'Investment');
                (events1.length).should.equal(1);
                (events1[0].args.id).should.equal(projectNative.projectId);
                (events1[0].args.investor).should.equal(owner3);
                (events1[0].args.value).should.eq.BN(value);
                (await pr.methods['projectBalance(bytes32)'].call(projectNative.projectId)).should.eq.BN(value);
                (await web3.eth.getBalance(pr.address)).should.eq.BN(web3.utils.toBN(registryBalance1).add(value));
                
                const registryAssetBalance = await projectErc20.token.methods['balanceOf(address)'].call(pr.address);
                await projectErc20.token.methods['approve(address,uint256)'].sendTransaction(pr.address, value, { from: owner3 });                
                const result2 = await pr.methods['invest(bytes32,uint256)'].sendTransaction(
                    projectErc20.projectId,
                    value,
                    { from: owner3 }
                );
                
                const events2 = result2.logs.filter(l => l.event === 'Investment');
                (events2.length).should.equal(1);
                (events2[0].args.id).should.equal(projectErc20.projectId);
                (events2[0].args.investor).should.equal(owner3);
                (events2[0].args.value).should.eq.BN(value);
                (await pr.methods['projectBalance(bytes32)'].call(projectErc20.projectId)).should.eq.BN(value);
                (await projectErc20.token.methods['balanceOf(address)'].call(pr.address)).should.eq.BN(registryAssetBalance.add(value));
            });

            it('should fail if zero value provided', async () => {
                await assertRevert(pr.methods['invest(bytes32,uint256)'].sendTransaction(
                    projectNative.projectId,
                    zeroAddress,
                    {
                        from: owner3
                    }
                ), 'NON_ZERO_VALUE_REQUIRED');
            });

            it('should fail if uknown project Id provided', async () => {
                const value = web3.utils.toBN(web3.utils.toWei('10', 'ether'));
                await assertRevert(pr.methods['invest(bytes32,uint256)'].sendTransaction(
                    web3.utils.asciiToHex('unknownId'),
                    value,
                    {
                        from: owner3
                    }
                ), 'PROJECT_NOT_FUND');
            });

            it('should fail if hardcap reached', async () => {
                const firstInvestment = web3.utils.toBN('170000000000000000000');
                await pr.methods['invest(bytes32,uint256)'].sendTransaction(
                    projectNative.projectId,
                    firstInvestment,
                    {
                        from: owner3,
                        value: firstInvestment
                    }
                );
                const secondInvestment = web3.utils.toBN(web3.utils.toWei('10', 'ether'));
                await assertRevert(pr.methods['invest(bytes32,uint256)'].sendTransaction(
                    projectNative.projectId,
                    secondInvestment,
                    {
                        from: owner4,
                        value: web3.utils.toBN(web3.utils.toWei('10', 'ether'))
                    }
                ), 'HARDCAP_REACHED');
            });

            it('should fail if investor has insufficient balance of native asset', async () => {
                const value = web3.utils.toBN(web3.utils.toWei('10', 'ether'));
                await assertRevert(pr.methods['invest(bytes32,uint256)'].sendTransaction(
                    projectNative.projectId,
                    value.add(web3.utils.toBN('100')),
                    {
                        from: owner3,
                        value
                    }
                ), 'INSUFFICIENT_BALANCE');
            });

            it('should fail if investor has insufficient balance of Erc20 asset', async () => {
                const value = await projectErc20.token.methods['balanceOf(address)'].call(owner3);   
                await assertRevert(pr.methods['invest(bytes32,uint256)'].sendTransaction(
                    projectErc20.projectId,
                    value.add(web3.utils.toBN('10')),
                    {
                        from: owner3
                    }
                ), 'INSUFFICIENT_BALANCE');
            });

            it('should fail if investor not allowed Erc20 asset transfer to the root contract', async () => {
                const value = web3.utils.toBN(web3.utils.toWei('10', 'ether'));
                await assertRevert(pr.methods['invest(bytes32,uint256)'].sendTransaction(
                    projectErc20.projectId,
                    value,
                    { from: owner3 }
                ), 'INSUFFICIENT_ALLOWANCE');
            });

            it('should fail if project with native asset not started yet', async () => {
                const projectNativeNotStarted = await createProjectWithNativeAsset(pr, ar, owner1, null, Math.round(Date.now()/1000) + 1000, null, true);
                const value = web3.utils.toBN(web3.utils.toWei('10', 'ether'));
                await assertRevert(pr.methods['invest(bytes32,uint256)'].sendTransaction(
                    projectNativeNotStarted.projectId,
                    value.add(web3.utils.toBN('100')),
                    {
                        from: owner3,
                        value
                    }
                ), 'NOT_ALLOWED_IN_CURRENT_STATE');
            });

            it('should fail if project with Erc20 asset not started yet', async () => {
                const projectErc20NotStarted = await createProjectWithErc20Asset(pr, ar, owner1, [owner3], null, Math.round(Date.now()/1000) + 1000, null, true);
                const value = await projectErc20.token.methods['balanceOf(address)'].call(owner3);
                await assertRevert(pr.methods['invest(bytes32,uint256)'].sendTransaction(
                    projectErc20NotStarted.projectId,
                    value.add(web3.utils.toBN('100')),
                    {
                        from: owner3
                    }
                ), 'NOT_ALLOWED_IN_CURRENT_STATE');
            });

            it('should fail if project with native asset campaign not started yet', async () => {
                const projectNativeNotStartedCampaign = await createProjectWithNativeAsset(pr, ar, owner1, null, null, null, true);
                const value = web3.utils.toBN(web3.utils.toWei('10', 'ether'));
                await assertRevert(pr.methods['invest(bytes32,uint256)'].sendTransaction(
                    projectNativeNotStartedCampaign.projectId,
                    value.add(web3.utils.toBN('100')),
                    {
                        from: owner3,
                        value
                    }
                ), 'NOT_ALLOWED_IN_CURRENT_STATE');
            });

            it('should fail if project with Erc20 asset campaign not started yet', async () => {
                const projectErc20NotStartedCampaign = await createProjectWithErc20Asset(pr, ar, owner1, [owner3], null, null, null, true);
                const value = await projectErc20.token.methods['balanceOf(address)'].call(owner3);
                await assertRevert(pr.methods['invest(bytes32,uint256)'].sendTransaction(
                    projectErc20NotStartedCampaign.projectId,
                    value.add(web3.utils.toBN('100')),
                    {
                        from: owner3
                    }
                ), 'NOT_ALLOWED_IN_CURRENT_STATE');
            });

            it('should fail if project with native asset has been started', async () => {
                const projectNativeExpired = await createProjectWithNativeAsset(prTime, ar, owner1, null, startDate, stopDate);
                // set project time to the project startDate + 10sec
                await prTime.methods['setCurrentTime(uint256)'].sendTransaction(
                    web3.utils.toBN(startDate + 10),
                    {
                        from: owner1
                    }
                );
                const value = web3.utils.toBN(web3.utils.toWei('10', 'ether'));
                await assertRevert(prTime.methods['invest(bytes32,uint256)'].sendTransaction(
                    projectNativeExpired.projectId,
                    value,
                    {
                        from: owner3,
                        value
                    }
                ), 'PROJECT_STARTED');
            });

            it('should fail if project with Erc20 asset has been started', async () => {
                const projectErc20Expired = await createProjectWithErc20Asset(prTime, ar, owner1, [owner3], null, startDate, stopDate);
                // set project time to the project startDate + 10sec
                await prTime.methods['setCurrentTime(uint256)'].sendTransaction(
                    web3.utils.toBN(startDate + 10),
                    {
                        from: owner1
                    }
                );
                const value = await projectErc20.token.methods['balanceOf(address)'].call(owner3);
                await assertRevert(prTime.methods['invest(bytes32,uint256)'].sendTransaction(
                    projectErc20Expired.projectId,
                    value,
                    {
                        from: owner3
                    }
                ), 'PROJECT_STARTED');
            });

            it('should fail if project with native asset has been expired', async () => {
                const projectNativeExpired = await createProjectWithNativeAsset(prTime, ar, owner1, null, startDate, stopDate);
                // set project time to the project stopDate + 10sec
                await prTime.methods['setCurrentTime(uint256)'].sendTransaction(
                    web3.utils.toBN(stopDate + 10),
                    {
                        from: owner1
                    }
                );
                const value = web3.utils.toBN(web3.utils.toWei('10', 'ether'));
                await assertRevert(prTime.methods['invest(bytes32,uint256)'].sendTransaction(
                    projectNativeExpired.projectId,
                    value,
                    {
                        from: owner3,
                        value
                    }
                ), 'PROJECT_EXPIRED');
            });

            it('should fail if project with Erc20 asset hes been expired', async () => {
                const projectErc20Expired = await createProjectWithErc20Asset(prTime, ar, owner1, [owner3], null, startDate, stopDate);
                // set project time to the project stopDate + 10sec
                await prTime.methods['setCurrentTime(uint256)'].sendTransaction(
                    web3.utils.toBN(stopDate + 10),
                    {
                        from: owner1
                    }
                );
                const value = await projectErc20.token.methods['balanceOf(address)'].call(owner3);
                await assertRevert(prTime.methods['invest(bytes32,uint256)'].sendTransaction(
                    projectErc20Expired.projectId,
                    value,
                    {
                        from: owner3
                    }
                ), 'PROJECT_EXPIRED');
            });
        });

        describe('#investFrom', () => {
            let projectNative;

            beforeEach(async () => {
                projectNative = await createProjectWithNativeAsset(pr, ar, owner1);
            });

            it('should fail if wrong investor (sender) address provided', async () => {
                const value = web3.utils.toBN(web3.utils.toWei('10', 'ether'));
                await assertRevert(pr.methods['investFrom(bytes32,address,uint256)'].sendTransaction(
                    projectNative.projectId,
                    zeroAddress,
                    value,
                    {
                        from: owner3,
                        value
                    }
                ), 'WRONG_FROM_ADDRESS');
            });

            it('should fail if zero value provided', async () => {
                await assertRevert(pr.methods['investFrom(bytes32,address,uint256)'].sendTransaction(
                    projectNative.projectId,
                    owner3,
                    zeroAddress,
                    {
                        from: owner3
                    }
                ), 'NON_ZERO_VALUE_REQUIRED');
            });

            it('should fail if project not found', async () => {
                const value = web3.utils.toBN(web3.utils.toWei('10', 'ether'));
                await assertRevert(pr.methods['investFrom(bytes32,address,uint256)'].sendTransaction(
                    web3.utils.asciiToHex('test'),
                    owner3,
                    value,
                    {
                        from: owner3,
                        value
                    }
                ), 'PROJECT_NOT_FUND');
            });

            it('should fail if hardcap reached', async () => {
                const firstInvestment = web3.utils.toBN('170000000000000000000');
                await pr.methods['investFrom(bytes32,address,uint256)'].sendTransaction(
                    projectNative.projectId,
                    owner3,
                    firstInvestment,
                    {
                        from: owner3,
                        value: firstInvestment
                    }
                );
                const secondInvestment = web3.utils.toBN(web3.utils.toWei('10', 'ether'));
                await assertRevert(pr.methods['investFrom(bytes32,address,uint256)'].sendTransaction(
                    projectNative.projectId,
                    owner3,
                    secondInvestment,
                    {
                        from: owner3,
                        secondInvestment
                    }
                ), 'HARDCAP_REACHED');
            });
        });

        describe('#rageQuit', () => {
            let projectNative;
            let projectErc20;

            beforeEach(async () => {
                projectNative = await createProjectWithNativeAsset(pr, ar, owner1);
                projectErc20 = await createProjectWithErc20Asset(pr, ar, owner1, [owner3, owner4, owner5]);
            });

            it('should allow withdraw funds', async () => {
                await makeInvestment(pr, ar, projectNative.projectId, '10', owner3);
                await makeInvestment(pr, ar, projectNative.projectId, '100', owner4);
                await makeInvestment(pr, ar, projectNative.projectId, '50', owner5);
                await makeInvestment(pr, ar, projectErc20.projectId, '10', owner3);
                await makeInvestment(pr, ar, projectErc20.projectId, '100', owner4);
                await makeInvestment(pr, ar, projectErc20.projectId, '50', owner5);

                const quitValue = web3.utils.toBN(web3.utils.toWei('10', 'ether'));

                const result1 = await pr.methods['rageQuit(bytes32,uint256)'].sendTransaction(
                    projectNative.projectId, 
                    quitValue,
                    {
                        from: owner3
                    }
                );
                const events1 = result1.logs.filter(l => l.event === 'RageQuit');
                (events1.length).should.equal(1);
                (events1[0].args.id).should.equal(projectNative.projectId);
                (events1[0].args.investor).should.equal(owner3);
                (events1[0].args.value).should.eq.BN(quitValue);
                
                const result2 = await pr.methods['rageQuit(bytes32,uint256)'].sendTransaction(
                    projectErc20.projectId, 
                    quitValue,
                    {
                        from: owner3
                    }
                );
                const events2 = result2.logs.filter(l => l.event === 'RageQuit');
                (events2.length).should.equal(1);
                (events2[0].args.id).should.equal(projectErc20.projectId);
                (events2[0].args.investor).should.equal(owner3);
                (events2[0].args.value).should.eq.BN(quitValue);
            });

            // @todo Add success test for the Voting state

            it('should fail if project not found', async () => {
                const quitValue = web3.utils.toBN(web3.utils.toWei('10', 'ether'));
                await assertRevert(pr.methods['rageQuit(bytes32,uint256)'].sendTransaction(
                    web3.utils.asciiToHex('unknownId'),
                    quitValue,
                    {
                        from: owner3
                    }
                ), 'PROJECT_NOT_FUND');
            });

            it('should fail if ragequit not allowed in Started state', async () => {
                await makeInvestment(pr, ar, projectNative.projectId, '10', owner3);
                await makeInvestment(pr, ar, projectNative.projectId, '100', owner4);
                await makeInvestment(pr, ar, projectNative.projectId, '50', owner5);
                await makeInvestment(pr, ar, projectErc20.projectId, '10', owner3);
                await makeInvestment(pr, ar, projectErc20.projectId, '100', owner4);
                await makeInvestment(pr, ar, projectErc20.projectId, '50', owner5);

                await pr.methods['startProject(bytes32)'].sendTransaction(projectNative.projectId, {
                    from: owner1
                });
                await pr.methods['startProject(bytes32)'].sendTransaction(projectErc20.projectId, {
                    from: owner1
                });

                const quitValue = web3.utils.toBN(web3.utils.toWei('10', 'ether'));

                await assertRevert(pr.methods['rageQuit(bytes32,uint256)'].sendTransaction(
                    projectNative.projectId, 
                    quitValue,
                    {
                        from: owner3
                    }
                ), 'RAGE_QUIT_NOT_ALLOWED');
                await assertRevert(pr.methods['rageQuit(bytes32,uint256)'].sendTransaction(
                    projectErc20.projectId, 
                    quitValue,
                    {
                        from: owner3
                    }
                ), 'RAGE_QUIT_NOT_ALLOWED');
            });

            it('should fail if insuficient funds', async () => {
                await makeInvestment(pr, ar, projectNative.projectId, '10', owner3);
                await makeInvestment(pr, ar, projectNative.projectId, '100', owner4);
                await makeInvestment(pr, ar, projectNative.projectId, '50', owner5);
                await makeInvestment(pr, ar, projectErc20.projectId, '10', owner3);
                await makeInvestment(pr, ar, projectErc20.projectId, '100', owner4);
                await makeInvestment(pr, ar, projectErc20.projectId, '50', owner5);

                const quitValue = web3.utils.toBN(web3.utils.toWei('20', 'ether'));

                await assertRevert(pr.methods['rageQuit(bytes32,uint256)'].sendTransaction(
                    projectNative.projectId, 
                    quitValue,
                    {
                        from: owner3
                    }
                ), 'INSUFICIENT_FUNDS');
                await assertRevert(pr.methods['rageQuit(bytes32,uint256)'].sendTransaction(
                    projectErc20.projectId, 
                    quitValue,
                    {
                        from: owner3
                    }
                ), 'INSUFICIENT_FUNDS');
            });
        });
    });

    describe('Voting process', () => {
        let projectNative;
        let projectErc20;
        const milestones = [
            {
                duration: web3.utils.toBN('100'),
                budget: web3.utils.toBN(web3.utils.toWei('30', 'ether'))
            },
            {
                duration: web3.utils.toBN('150'),
                budget: web3.utils.toBN(web3.utils.toWei('50', 'ether'))
            },
            {
                duration: web3.utils.toBN('200'),
                budget: web3.utils.toBN(web3.utils.toWei('60', 'ether'))
            }
        ];
        const investments = [
            {
                investor: owner3,
                value: '10'
            },
            {
                investor: owner4,
                value: '100'
            },
            {
                investor: owner5,
                value: '50'
            }
        ];
        const voteValue = web3.utils.toBN('100');

        beforeEach(async () => {
            projectNative = await createProjectWithNativeAsset(pr, ar, owner1, milestones);
            projectErc20 = await createProjectWithErc20Asset(pr, ar, owner1, [owner3, owner4, owner5], milestones);
            await doInvestments(pr, ar, projectNative.projectId, owner1, investments, true);
            await doInvestments(pr, ar, projectErc20.projectId, owner1, investments, true);
            await pr.methods['withdraw(bytes32)'].sendTransaction(
                projectNative.projectId,
                {
                    from: owner1
                }
            );
            await pr.methods['withdraw(bytes32)'].sendTransaction(
                projectErc20.projectId,
                {
                    from: owner1
                }
            );
            await pr.methods['publishReport(bytes32,bytes32,uint8)'].sendTransaction(
                projectNative.projectId,
                web3.utils.asciiToHex('querty'),
                UrlType.Ipfs,
                {
                    from: owner1
                }
            );
            await pr.methods['publishReport(bytes32,bytes32,uint8)'].sendTransaction(
                projectErc20.projectId,
                web3.utils.asciiToHex('querty'),
                UrlType.Ipfs,
                {
                    from: owner1
                }
            );
        });      

        describe('#vote', () => {

            it('should fail if project not existed', async () => {
                await assertRevert(pr.methods['vote(bytes32,uint256)'].sendTransaction(
                    web3.utils.asciiToHex('test'),
                    voteValue,
                    {
                        from: owner3
                    }
                ), 'PROJECT_NOT_FUND');
            });
            
            it('should fail of service is paused', async () => {
                await ar.methods['pause()'].sendTransaction({
                    from: owner1
                });
                await assertRevert(pr.methods['vote(bytes32,uint256)'].sendTransaction(
                    projectNative.projectId,
                    voteValue,
                    {
                        from: owner3
                    }
                ), 'SERVICE_PAUSED');
            });

            it('should fail in case of reentrant call', async () => {
                const rc = await ProjectRegistryReentracyTests.new();
                await assertRevert(rc.methods['voteReentracy(address,bytes32,uint256)'].sendTransaction(
                    pr.address,
                    projectNative.projectId,
                    voteValue,
                    {
                        from: owner3
                    }
                ));
            });

            it('should fail if called by not an investor', async () => {
                await assertRevert(pr.methods['vote(bytes32,uint256)'].sendTransaction(
                    projectNative.projectId,
                    voteValue,
                    {
                        from: owner1
                    }
                ), 'NOT_AN_INVESTOR');
            });

            it('should fail if called not in Voting state', async () => {
                const projectNativeNotVoting = await createProjectWithNativeAsset(pr, ar, owner1);
                await doInvestments(pr, ar, projectNativeNotVoting.projectId, owner1, investments, true);
                await pr.methods['withdraw(bytes32)'].sendTransaction(
                    projectNativeNotVoting.projectId,
                    {
                        from: owner1
                    }
                );

                await assertRevert(pr.methods['vote(bytes32,uint256)'].sendTransaction(
                    projectNativeNotVoting.projectId,
                    voteValue,
                    {
                        from: owner3
                    }
                ), 'NOT_ALLOWED_IN_CURRENT_STATE');
            });

            it('should fail if voter have insufficient tokens balance', async () => {
                const balance = await token.methods['balanceOf(address)'].call(owner3);
                await assertRevert(pr.methods['vote(bytes32,uint256)'].sendTransaction(
                    projectNative.projectId,
                    balance.add(voteValue),
                    {
                        from: owner3
                    }
                ), 'INSUFICIENT_FUNDS');
            });

            it('should register a vote', async () => {
                const result = await pr.methods['vote(bytes32,uint256)'].sendTransaction(
                    projectNative.projectId,
                    voteValue,
                    {
                        from: owner3
                    }
                );
                const events = result.logs.filter(l => l.event === 'Voted');
                (events.length).should.equal(1);
                (events[0].args.id).should.equal(projectNative.projectId);
                (events[0].args.investor).should.equal(owner3);
                (events[0].args.value).should.eq.BN(voteValue);
            });
        });

        describe('#votingResult', () => {

            it('should fail if project not existed', async () => {
                await assertRevert(pr.methods['votingResult(bytes32)'].call(
                    web3.utils.asciiToHex('test')
                ), 'PROJECT_NOT_FUND');
            });

            it('should fail if project not in Voting state', async () => {
                const projectNativeNotVoting = await createProjectWithNativeAsset(pr, ar, owner1);
                await doInvestments(pr, ar, projectNativeNotVoting.projectId, owner1, investments, true);
                await pr.methods['withdraw(bytes32)'].sendTransaction(
                    projectNativeNotVoting.projectId,
                    {
                        from: owner1
                    }
                );

                await assertRevert(pr.methods['votingResult(bytes32)']
                    .call(projectNativeNotVoting.projectId), 'NOT_ALLOWED_IN_CURRENT_STATE');
            });

            it('should return voting result', async () => {
                const project = await pr.methods['getProject(bytes32)'].call(projectNative.projectId);
                const asset = await ar.methods['getAsset(bytes32)'].call(project.assetId);
                const projectBalance = await pr.methods['projectBalance(bytes32)'].call(projectNative.projectId);
                await pr.methods['vote(bytes32,uint256)'].sendTransaction(
                    projectNative.projectId,
                    voteValue,
                    {
                        from: owner3
                    }
                );
                const threshold = calculateVotingThreshold(
                    projectBalance,
                    '0',
                    milestones,
                    '1',
                    feeRate,
                    project.softCap,
                    asset.nativeFactor
                );
                const result = await pr.methods['votingResult(bytes32)'].call(projectNative.projectId);
                (result.value).should.eq.BN(isqrt(voteValue));
                (result.threshold).should.eq.BN(threshold);
            });
        });

        describe('#closeVoting', () => {

            beforeEach(async () => {
                await pr.methods['vote(bytes32,uint256)'].sendTransaction(
                    projectNative.projectId,
                    voteValue,
                    {
                        from: owner3
                    }
                );
            });

            it('should fail if project not existed', async () => {
                await assertRevert(pr.methods['closeVoting(bytes32)'].sendTransaction(
                    web3.utils.asciiToHex('test'),
                    {
                        from: owner1
                    }
                ), 'PROJECT_NOT_FUND');
            });

            it('should fail if service paused', async () => {
                await ar.methods['pause()'].sendTransaction({
                    from: owner1
                });
                await assertRevert(pr.methods['closeVoting(bytes32)'].sendTransaction(
                    projectNative.projectId,
                    {
                        from: owner1
                    }
                ), 'SERVICE_PAUSED');
            });

            it('should fail if called by not an owner', async () => {
                await assertRevert(pr.methods['closeVoting(bytes32)'].sendTransaction(
                    projectNative.projectId,
                    {
                        from: owner3
                    }
                ), 'NOT_AN_OWNER');
            });

            it('should fail if called not in Voting state', async () => {
                const projectNativeNotVoting = await createProjectWithNativeAsset(pr, ar, owner1);
                await doInvestments(pr, ar, projectNativeNotVoting.projectId, owner1, investments, true);
                await pr.methods['withdraw(bytes32)'].sendTransaction(
                    projectNativeNotVoting.projectId,
                    {
                        from: owner1
                    }
                );
                await assertRevert(pr.methods['closeVoting(bytes32)'].sendTransaction(
                    projectNativeNotVoting.projectId,
                    {
                        from: owner1
                    }
                ), 'NOT_ALLOWED_IN_CURRENT_STATE');
            });

            it('should move project to the Started state (on voting success)', async () => {
                const balance4 = await token.methods['balanceOf(address)'].call(owner4);
                const balance5 = await token.methods['balanceOf(address)'].call(owner5);
                await pr.methods['vote(bytes32,uint256)'].sendTransaction(
                    projectNative.projectId,
                    balance4,
                    {
                        from: owner4
                    }
                );
                await pr.methods['vote(bytes32,uint256)'].sendTransaction(
                    projectNative.projectId,
                    balance5,
                    {
                        from: owner5
                    }
                );
                const result = await pr.methods['closeVoting(bytes32)'].sendTransaction(
                    projectNative.projectId,
                    {
                        from: owner1
                    }
                );
                const events = result.logs.filter(l => l.event === 'ProjectStateChanged');
                (events.length).should.equal(1);
                (events[0].args.id).should.equal(projectNative.projectId);
                (events[0].args.newState.toNumber()).should.equal(ProjectState.Started);
                (events[0].args.oldState.toNumber()).should.equal(ProjectState.Voting);

            });

            it('should move project to the VotingFailed state (on voting failure)', async () => {
                const result = await pr.methods['closeVoting(bytes32)'].sendTransaction(
                    projectNative.projectId,
                    {
                        from: owner1
                    }
                );
                const events = result.logs.filter(l => l.event === 'ProjectStateChanged');
                (events.length).should.equal(1);
                (events[0].args.id).should.equal(projectNative.projectId);
                (events[0].args.newState.toNumber()).should.equal(ProjectState.VotingFailed);
                (events[0].args.oldState.toNumber()).should.equal(ProjectState.Voting);
            });
        });
    });
});
