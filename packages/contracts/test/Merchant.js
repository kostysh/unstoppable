require('chai')
    .use(require('bn-chai')(web3.utils.BN))
    .should();

const assertRevert = require('./helpers/assertRevert');
const {
    createProjectWithNativeAsset
} = require('./helpers/projects');
const { zeroAddress } = require('./helpers/constants');

const UNFToken = artifacts.require('UNFToken');
const Unstoppable = artifacts.require('Unstoppable');
const ProjectsRegistry = artifacts.require('ProjectsRegistry');
const Merchant = artifacts.require('Merchant');

contract('Merchant', ([owner1, owner2, owner3, owner4]) => {
    let token;
    let ar;
    let pr;
    let mr;
    let projectNative;

    beforeEach(async () => {
        token = await UNFToken.new(owner1, web3.utils.toBN(web3.utils.toWei('1000000', 'ether')), {
            from: owner1
        });
        ar = await Unstoppable.new({
            from: owner1
        });
        pr = await ProjectsRegistry.new(ar.address, token.address, {
            from: owner1
        });
        mr = await Merchant.new(pr.address, {
            from: owner1
        });
        projectNative = await createProjectWithNativeAsset(pr, ar, owner1);
    });

    describe('Fallback function', () => {

        it('should accept raw payments and emit acceptance events', async () => {
            (await web3.eth.getBalance(mr.address)).should.eq.BN(web3.utils.toBN('0'));
            const value = web3.utils.toBN(web3.utils.toWei('1', 'ether'));
            await web3.eth.sendTransaction({
                from: owner2,
                to: mr.address,
                value
            });
            await web3.eth.sendTransaction({
                from: owner3,
                to: mr.address,
                value
            });
            const o2 = await mr.methods['balanceOf(address)'].call(owner2);
            const o3 = await mr.methods['balanceOf(address)'].call(owner3);
            (o2).should.eq.BN(value);
            (o3).should.eq.BN(value);
            (await web3.eth.getBalance(mr.address)).should.eq.BN(o2.add(o3));
            const events = await mr.getPastEvents('PaymentReceived', {
                fromBlock: 0
            });
            (events.length).should.equal(2);
            (events[0].args.from).should.equal(owner2);
            (events[0].args.value).should.eq.BN(value);
            (events[1].args.from).should.equal(owner3);
            (events[1].args.value).should.eq.BN(value);
        });
    });

    describe('Public methods', () => {
        let value;

        beforeEach(async () => {
            value = web3.utils.toBN(web3.utils.toWei('1', 'ether'));
            await web3.eth.sendTransaction({
                from: owner2,
                to: mr.address,
                value
            });
            await web3.eth.sendTransaction({
                from: owner3,
                to: mr.address,
                value
            });
        });

        describe('#withdraw', () => {

            it('should withdraw funds', async () => {
                await mr.methods['withdraw(uint256)'].sendTransaction(value, {
                    from: owner2
                });
                const events = await mr.getPastEvents('Withdrawal', {
                    fromBlock: 0
                });
                (events.length).should.equal(1);
                (events[0].args.sender).should.equal(owner2);
                (events[0].args.value).should.eq.BN(value);
            });

            it('should fail if called by unknown sender', async () => {
                await assertRevert(mr.methods['withdraw(uint256)'].sendTransaction(value, {
                    from: owner4
                }), 'INSUFICIENT_FUNDS');
            });

            it('should fail if insufficient funds', async () => {
                await assertRevert(mr.methods['withdraw(uint256)'].sendTransaction(value.add(value), {
                    from: owner2
                }), 'INSUFICIENT_FUNDS');
            });
        });

        describe('#makeInvestment', () => {

            it('should make investment based on payment', async () => {
                await mr.methods['makeInvestment(bytes32,address,uint256)'].sendTransaction(
                    projectNative.projectId, 
                    owner2, 
                    value, 
                    {
                        from: owner1
                    }
                );
                const events = await mr.getPastEvents('InvestmentMade', {
                    fromBlock: 0
                });
                (events.length).should.equal(1);
                (events[0].args.projectId).should.equal(projectNative.projectId);
                (events[0].args.from).should.equal(owner2);
                (events[0].args.value).should.eq.BN(value);
                const events2 = await pr.getPastEvents('Investment', {
                    fromBlock: 0
                });
                (events2.length).should.equal(1);
                (await mr.methods['balanceOf(address)'].call(owner2)).should.eq.BN(web3.utils.toBN('0'));
            });

            it('should fail if called by not owner address', async () => {
                await assertRevert(mr.methods['makeInvestment(bytes32,address,uint256)'].sendTransaction(
                    projectNative.projectId, 
                    owner2, 
                    value, 
                    {
                        from: owner2
                    }
                ), 'Ownable: caller is not the owner');
            });

            it('should fail if zero from address provided', async () => {
                await assertRevert(mr.methods['makeInvestment(bytes32,address,uint256)'].sendTransaction(
                    projectNative.projectId, 
                    zeroAddress, 
                    value, 
                    {
                        from: owner1
                    }
                ), 'WRONG_FROM_ADDRESS');
            });

            it('should fail if unknown project Id has been provided', async () => {
                await assertRevert(mr.methods['makeInvestment(bytes32,address,uint256)'].sendTransaction(
                    web3.utils.asciiToHex('test'), 
                    owner2, 
                    value, 
                    {
                        from: owner1
                    }
                ), 'PROJECT_NOT_FOUND');
            });

            it('should fail if insuficient has been deposited', async () => {
                await assertRevert(mr.methods['makeInvestment(bytes32,address,uint256)'].sendTransaction(
                    projectNative.projectId, 
                    owner2, 
                    value.add(value), 
                    {
                        from: owner1
                    }
                ), 'INSUFICIENT_FUNDS');
            });

            it('should fail if address(projectsRegistry).call results with revert', async () => {
                const projectNativeNotStarted = await createProjectWithNativeAsset(pr, ar, owner1, null, null, null, true);
                // investment cannot be done if project has been closed
                await assertRevert(mr.methods['makeInvestment(bytes32,address,uint256)'].sendTransaction(
                    projectNativeNotStarted.projectId, 
                    owner2, 
                    value, 
                    {
                        from: owner1
                    }
                ), 'UNABLE_TO_PROCESS_PAYMENT');
            });
        });
    });
});