require('chai')
    .use(require('bn-chai')(web3.utils.BN))
    .should();

const { zeroAddress } = require('./helpers/constants');
const assertRevert = require('./helpers/assertRevert');
const UNFToken = artifacts.require('UNFToken');
const UNFTokenCaller = artifacts.require('UNFTokenCaller');

contract('UNFToken', ([owner1, owner2, owner3]) => {
    let tokensValue = web3.utils.toBN(web3.utils.toWei('1000000', 'ether'));
    let token;
    let tokenCaller;

    beforeEach(async () => {
        token = await UNFToken.new(owner1, tokensValue, {
            from: owner2
        });
        tokenCaller = await UNFTokenCaller.new(token.address, {
            from: owner2
        });
        await token.methods['addWhitelisted(address)'].sendTransaction(tokenCaller.address, {
            from: owner2
        });
    });

    describe('Constructor', () => {

        it('should fail if owner provided as zero address', async () => {
            await assertRevert(UNFToken.new(zeroAddress, tokensValue, {
                from: owner2
            }), 'ERC20: mint to the zero address');
        });

        it('should mint tokens for the owner', async () => {
            (await token.balanceOf(owner1)).should.eq.BN(tokensValue);
            const events = await token.getPastEvents('Minted', {
                fromBlock: 0
            });
            (events.length).should.equal(1);
            (events[0].args.minter).should.equal(owner2);
            (events[0].args.recipient).should.equal(owner1);
            (events[0].args.value).should.eq.BN(tokensValue);
        });
    });

    describe('Public methods', () => {

        describe('#mint', () => {

            it('should mint tokens to the address', async () => {
                await tokenCaller.methods['mint(address,uint256)'].sendTransaction(
                    owner3,
                    tokensValue,
                    {
                        from: owner2
                    }
                );
                (await token.balanceOf(owner3)).should.eq.BN(tokensValue);
                const events = await token.getPastEvents('Minted', {
                    fromBlock: 0
                });
                (events.length).should.equal(2);
                (events[1].args.minter).should.equal(tokenCaller.address);
                (events[1].args.recipient).should.equal(owner3);
                (events[1].args.value).should.eq.BN(tokensValue);
            });

            it('should fail if called by not whitelisted address', async () => {
                await assertRevert(token.methods['mint(address,uint256)'].sendTransaction(
                    owner3,
                    tokensValue,
                    {
                        from: owner2
                    }
                ), 'WhitelistedRole: caller does not have the Whitelisted role');
            });

            it('should fail if recipient address provided as zero address', async () => {
                await assertRevert(tokenCaller.methods['mint(address,uint256)'].sendTransaction(
                    zeroAddress,
                    tokensValue,
                    {
                        from: owner2
                    }
                ), 'ERC20: mint to the zero address');
            });

            it('should fail if tokens value provided as zero', async () => {
                await assertRevert(tokenCaller.methods['mint(address,uint256)'].sendTransaction(
                    owner3,
                    '0',
                    {
                        from: owner2
                    }
                ), 'ZERO_VALUE_NOT_ALLOWED');
            });
        });

        describe('#burn', () => {

            beforeEach(async () => {
                await tokenCaller.methods['mint(address,uint256)'].sendTransaction(
                    owner3,
                    tokensValue,
                    {
                        from: owner2
                    }
                );
            });

            it('should burn tokens', async () => {
                await tokenCaller.methods['burn(address,uint256)'].sendTransaction(
                    owner3,
                    tokensValue,
                    {
                        from: owner2
                    }
                );
                (await token.balanceOf(owner3)).should.eq.BN(web3.utils.toBN('0'));
                const events = await token.getPastEvents('Burned', {
                    fromBlock: 0
                });
                (events.length).should.equal(1);
                (events[0].args.burner).should.equal(tokenCaller.address);
                (events[0].args.owner).should.equal(owner3);
                (events[0].args.value).should.eq.BN(tokensValue);
            });

            it('should fail if called by not whitelisted address', async () => {
                await assertRevert(token.methods['burn(address,uint256)'].sendTransaction(
                    owner3,
                    tokensValue,
                    {
                        from: owner2
                    }
                ), 'WhitelistedRole: caller does not have the Whitelisted role');
            });

            it('should fail if from address provided as zero address', async () => {
                await assertRevert(tokenCaller.methods['burn(address,uint256)'].sendTransaction(
                    zeroAddress,
                    tokensValue,
                    {
                        from: owner2
                    }
                ), 'ERC20: burn from the zero address');
            });

            it('should fail if tokens to burn value provided as zero', async () => {
                await assertRevert(tokenCaller.methods['burn(address,uint256)'].sendTransaction(
                    owner3,
                    '0',
                    {
                        from: owner2
                    }
                ), 'ZERO_VALUE_NOT_ALLOWED');
            });
        });
    });
});
