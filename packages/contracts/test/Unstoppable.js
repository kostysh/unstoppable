require('chai')
    .use(require('bn-chai')(web3.utils.BN))
    .should();

const assertRevert = require('./helpers/assertRevert');
const { AssetsType, zeroAddress } = require('./helpers/constants');
const { calculateInvestmentTokens } = require('./helpers/economics');
const Unstoppable = artifacts.require('Unstoppable');
const Erc20Token = artifacts.require('Erc20Token');
const ERC165CheckerTests = artifacts.require('ERC165CheckerTests');
const UnstoppableReentracyTests = artifacts.require('UnstoppableReentracyTests');

contract('Unstoppable', ([owner1, owner2]) => {
    let un;    

    beforeEach(async () => {
        un = await Unstoppable.new({
            from: owner1
        });
    });

    describe('Constructor', () => {
        let erc165;
        
        beforeEach(async () => {
            erc165 = await ERC165CheckerTests.new({
                from: owner1
            });
        });

        it('contract should supports ERC165 interface', async () => {
            await erc165.methods['supportsInterface(address,bytes4)'].call(un.address, '0x3404764d');
        });
    });

    describe('Fallback function', () => {

        it('should accept incoming ether', async () => {
            const initialBalance = web3.utils.toBN(await web3.eth.getBalance(un.address));
            const value = web3.utils.toBN(web3.utils.toWei('1', 'ether'));

            await web3.eth.sendTransaction({
                to: un.address,
                from: owner1,
                value
            });

            (await web3.eth.getBalance(un.address)).should.eq.BN(initialBalance.add(value));
        });
    });

    describe('Public methods', () => {

        describe('#serviceFee()', () => {

            it('should return current service fee', async () => {
                (await un.methods['serviceFee()'].call()).should.eq.BN(web3.utils.toBN('500'));
            });
        });

        describe('#setFee(uint256)', () => {

            it('should set new fee value', async () => {
                const oldFee = await un.methods['serviceFee()'].call();
                const newFee = web3.utils.toBN('700');
                await un.methods['pause()'].sendTransaction({
                    from: owner1
                });
                const result = await un.methods['setFee(uint256)'].sendTransaction(
                    newFee,
                    {
                        from: owner1
                    }
                );
                const events = result.logs.filter(l => l.event === 'FeeUpdated');
                (events[0].args.fee).should.eq.BN(newFee);
                (events[0].args.oldFee).should.eq.BN(oldFee);
                (events[0].args.owner).should.equal(owner1);

                (await un.methods['serviceFee()'].call()).should.eq.BN(newFee);
            });

            it('should fail if called by not an owner', async () => {
                await un.methods['pause()'].sendTransaction({
                    from: owner1
                });
                await assertRevert(un.methods['setFee(uint256)'].sendTransaction(
                    web3.utils.toBN('700'),
                    {
                        from: owner2
                    }
                ), 'Ownable: caller is not the owner');
            });

            it('should fail if service not paused', async () => {
                await assertRevert(un.methods['setFee(uint256)'].sendTransaction(
                    web3.utils.toBN('700'),
                    {
                        from: owner1
                    }
                ), 'Pausable: not paused');
            });
        });

        describe('#calculateTokens(uint256,uint256,uint256)', () => {

            it('should calculate amount of tokens', async () => {
                const investmentValue = web3.utils.toBN(web3.utils.toWei('10', 'ether'));
                const nativeFactor = web3.utils.toBN('5521');
                const softCap = web3.utils.toBN(web3.utils.toWei('1000', 'ether'));

                (await un.methods['calculateTokens(uint256,uint256,uint256)'].call(investmentValue, softCap, nativeFactor))
                    .should.eq.BN(calculateInvestmentTokens(investmentValue, softCap, nativeFactor));
            });

            it('should return 1 (minimum value) if extremely small investments provided', async () => {
                const investmentValue = web3.utils.toBN(web3.utils.toWei('0.0005', 'ether'));
                const nativeFactor = web3.utils.toBN('5521');
                const softCap = web3.utils.toBN(web3.utils.toWei('1000', 'ether'));

                (await un.methods['calculateTokens(uint256,uint256,uint256)'].call(investmentValue, softCap, nativeFactor))
                    .should.eq.BN(web3.utils.toBN('1'));
            });
        });

        describe('#getInterfaceId()', () => {

            it('should return contracts ERC165 interface', async () => {
                (await un.methods['getInterfaceId()'].call()).should.equal('0x3404764d');                
            });
        });
    });

    describe('Owners methods', () => {
        let token;
        let nativeValue = web3.utils.toBN(web3.utils.toWei('20', 'ether'));
        let erc20Value = web3.utils.toBN(web3.utils.toWei('20', 'ether'));
        let nativeAssetId;
        let erc20AssetId;

        beforeEach(async () => {
            token = await Erc20Token.new('Test Coin', 'TEST', 18, web3.utils.toBN(web3.utils.toWei('1000000000', 'ether')), {
                from: owner2
            });
            await web3.eth.sendTransaction({
                to: un.address,
                from: owner1,
                value: nativeValue
            });
            await token.methods['transfer(address,uint256)'].sendTransaction(un.address, erc20Value, {
                from: owner2
            });
            await un.methods['pause()'].sendTransaction({
                from: owner1
            });
            const res1 = await un.methods['addAsset(uint8,address,uint256)'].sendTransaction(
                AssetsType.Native, 
                zeroAddress, 
                web3.utils.toBN('1000000'),
                {
                    from: owner1
                }
            );
            nativeAssetId = res1.logs.filter(l => l.event === 'AssetAdded')[0].args.id;
            const res2 = await un.methods['addAsset(uint8,address,uint256)'].sendTransaction(
                AssetsType.Erc20, 
                token.address, 
                web3.utils.toBN('5521'),
                {
                    from: owner1
                }
            );
            erc20AssetId = res2.logs.filter(l => l.event === 'AssetAdded')[0].args.id;
            await un.methods['unpause()'].sendTransaction({
                from: owner1
            });
        });
        
        describe('#withdraw(bytes32,uint256)', () => {

            describe('Native asset', () => {

                it('should withdraw asset', async () => {
                    const initialOwnerBalance = web3.utils.toBN(await web3.eth.getBalance(owner1));
                    const result = await un.methods['withdraw(bytes32,uint256)'].sendTransaction(
                        nativeAssetId,
                        nativeValue,
                        {
                            from: owner1
                        }
                    );
                    const tx = await web3.eth.getTransactionFromBlock('pending', result.receipt.transactionIndex);
                    const gasPrice = web3.utils.toBN(web3.utils.toBN(tx.gasPrice));
                    const events = result.logs.filter(l => l.event === 'WithdrawFee');
                    (events[0].args.id).should.equal(nativeAssetId);
                    (events[0].args.owner).should.equal(owner1);
                    (events[0].args.value).should.eq.BN(nativeValue);
                    
                    (await web3.eth.getBalance(owner1)).should.eq.BN(
                        initialOwnerBalance
                            .add(nativeValue)
                            .sub(web3.utils.toBN(result.receipt.gasUsed).mul(gasPrice))
                    );
                });

                it('should fail if unknown assetId has been provided', async () => {
                    await assertRevert(un.methods['withdraw(bytes32,uint256)'].sendTransaction(
                        web3.utils.asciiToHex('test'),
                        nativeValue,
                        {
                            from: owner1
                        }
                    ), 'ASSET_NOT_FOUND');
                });

                it('should fail if insuficient funds requested', async () => {
                    await assertRevert(un.methods['withdraw(bytes32,uint256)'].sendTransaction(
                        nativeAssetId,
                        nativeValue.add(nativeValue),
                        {
                            from: owner1
                        }
                    ), 'INSUFICIENT_FUNDS');
                });

                it('should fail in case of reentrant call', async () => {
                    const pawner = await UnstoppableReentracyTests.new({
                        from: owner1
                    });
                    await un.methods['transferOwnership(address)'].sendTransaction(pawner.address, {
                        from: owner1
                    });
                    await assertRevert(pawner.methods['withdrawEther(address,bytes32,uint256)'].sendTransaction(
                        un.address,
                        nativeAssetId,
                        nativeValue,
                        {
                            from: owner1
                        }
                    ));
                });
            });

            describe('ERC20 asset', () => {

                it('should withdraw asset', async () => {
                    const initialOwnerBalance = await token.methods['balanceOf(address)'].call(owner1);
                    const result = await un.methods['withdraw(bytes32,uint256)'].sendTransaction(
                        erc20AssetId,
                        erc20Value,
                        {
                            from: owner1
                        }
                    );
                    const events = result.logs.filter(l => l.event === 'WithdrawFee');
                    (events[0].args.id).should.equal(erc20AssetId);
                    (events[0].args.owner).should.equal(owner1);
                    (events[0].args.value).should.eq.BN(erc20Value);
                    
                    (await token.methods['balanceOf(address)'].call(owner1)).should.eq.BN(
                        initialOwnerBalance
                            .add(erc20Value)
                    );
                });

                it('should fail if insuficient funds requested', async () => {
                    await assertRevert(un.methods['withdraw(bytes32,uint256)'].sendTransaction(
                        erc20AssetId,
                        erc20Value.add(erc20Value),
                        {
                            from: owner1
                        }
                    ), 'INSUFICIENT_FUNDS');
                });

                it('should fail in case of reentrant call', async () => {
                    const pawner = await UnstoppableReentracyTests.new({
                        from: owner1
                    });
                    await un.methods['transferOwnership(address)'].sendTransaction(pawner.address, {
                        from: owner1
                    });
                    await assertRevert(pawner.methods['withdrawErc20(address,bytes32,uint256)'].sendTransaction(
                        un.address,
                        erc20AssetId,
                        erc20Value,
                        {
                            from: owner1
                        }
                    ));
                });
            });
        });
    });
});