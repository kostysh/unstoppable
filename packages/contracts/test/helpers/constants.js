module.exports.zeroAddress = '0x0000000000000000000000000000000000000000';

module.exports.AssetsType = {
    Native: 0,
    Erc20: 1
};

module.exports.UrlType = {
    Ipfs: 0,
    Swarm: 1,
    Arweave: 2
};

module.exports.ProjectState = {
    Uninitialized: 0,
    Created: 1,
    Closed: 2,
    Campaign: 3,
    CampaignFailed: 4,
    Started: 5,
    Voting: 6,
    VotingFailed: 7
};
