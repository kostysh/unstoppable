const { AssetsType, zeroAddress, UrlType } = require('./constants');
const Erc20Token = artifacts.require('Erc20Token');

/**
 * Extract specific property into array 
 * @param {Object} source Source object
 * @param {string} property Property to extract
 * @returns {*[]}
 */
const unwindBy = (source, property) => source.map(r => r[property]);
module.exports.unwindBy= unwindBy;

/**
 * Add new project
 * @param {Object} contract ProjectsRegistry instance
 * @param {string} from Owner address
 * @param {string} url Description url
 * @param {number} urlType Description url type
 * @param {string} assetId Asset Id
 * @param {string} softCap SoftCap value
 * @param {string} hardCap HardCap value
 * @param {number} startDate Start date unixtime
 * @param {number} stopDate Stop date unixtime
 * @param {Object} milestones Milestones config
 * @returns {Promise<Object>} Transaction result
 */
const addProject = (
    contract, 
    from, 
    url, 
    urlType, 
    assetId, 
    hardCap, 
    startDate, 
    stopDate, 
    milestones,
    simulateInconsistance = false
) => contract
    .methods['addProject(bytes32,uint8,bytes32,uint256,uint256,uint256,uint256[],uint256[])']
    .sendTransaction(
        web3.utils.asciiToHex(url), 
        urlType,
        assetId,
        web3.utils.toBN(hardCap),
        web3.utils.toBN(startDate),
        web3.utils.toBN(stopDate),
        unwindBy(milestones, 'duration'),
        unwindBy(milestones, 'budget').slice(simulateInconsistance ? 1 : 0),
        {
            from
        }
    );
module.exports.addProject = addProject;

/**
 * Update existed project
 * @param {string} id Project Id
 * @param {Object} contract ProjectsRegistry instance
 * @param {string} from Owner address
 * @param {string} url Description url
 * @param {number} urlType Description url type
 * @param {string} assetId Asset Id
 * @param {string} hardCap HardCap value
 * @param {number} startDate Start date unixtime
 * @param {number} stopDate Stop date unixtime
 * @param {Object} milestones Milestones config
 * @returns {Promise<Object>} Transaction result
 */
const updateProject = (
    contract, 
    id,
    from, 
    url, 
    urlType, 
    assetId, 
    hardCap, 
    startDate, 
    stopDate, 
    milestones,
    simulateInconsistance = false
) => contract
    .methods['updateProject(bytes32,bytes32,uint8,bytes32,uint256,uint256,uint256,uint256[],uint256[])']
    .sendTransaction(
        id,
        web3.utils.asciiToHex(url), 
        urlType,
        assetId,
        web3.utils.toBN(hardCap),
        web3.utils.toBN(startDate),
        web3.utils.toBN(stopDate),
        unwindBy(milestones, 'duration'),
        unwindBy(milestones, 'budget').slice(simulateInconsistance ? 1 : 0),
        {
            from
        }
    );
module.exports.updateProject = updateProject;

/**
 * Create project with native asset
 * @param {Object} projectsContract ProjectsRegistry contract instance
 * @param {Object} assetsContract AssetsRegistry contract instance
 * @param {string} projectsContractOwner Contracts owner
 * @param {Object|null|undefined} milestonesOverride
 * @param {number|null|undefined} startDateOverride
 * @param {number"null|undefined} stopDateOverride
 * @param {Bollean} notStartedCampaign
 * @returns {Promise<Object>} Asset and project Ids {assetId,projectId}
 */
module.exports.createProjectWithNativeAsset = async (
    projectsContract,
    assetsContract, 
    projectsContractOwner,
    milestonesOverride,
    startDateOverride,
    stopDateOverride,
    notStartedCampaign = false
) => {
    let assetId;
    let projectId;

    await assetsContract.methods['pause()'].sendTransaction({
        from: projectsContractOwner
    });
    const result1 = await assetsContract.methods['addAsset(uint8,address,uint256)'].sendTransaction(
        AssetsType.Native, 
        zeroAddress, 
        web3.utils.toBN('1000000'),
        {
            from: projectsContractOwner
        }
    );
    await assetsContract.methods['unpause()'].sendTransaction({
        from: projectsContractOwner
    });
    const events1 = result1.logs.filter(l => l.event === 'AssetAdded');
    assetId = events1[0].args.id;    

    const milestones = milestonesOverride || [
        {
            duration: web3.utils.toBN('100'),
            budget: web3.utils.toBN(web3.utils.toWei('30', 'ether'))
        },
        {
            duration: web3.utils.toBN('150'),
            budget: web3.utils.toBN(web3.utils.toWei('50', 'ether'))
        },
        {
            duration: web3.utils.toBN('200'),
            budget: web3.utils.toBN(web3.utils.toWei('60', 'ether'))
        }
    ];
    const startDate = startDateOverride || Math.round(Date.now()/1000) + 10;//now + 10 sec
    const stopDate = stopDateOverride || startDate + (60*60*24*60);

    const result2 = await addProject(
        projectsContract,
        projectsContractOwner,
        'querty',
        UrlType.Ipfs,
        assetId,
        web3.utils.toWei('170', 'ether'),
        startDate,
        stopDate,
        milestones
    );
    
    const events2 = result2.logs.filter(l => l.event === 'ProjectAdded');
    projectId = events2[0].args.id;

    if (!notStartedCampaign) {
        await projectsContract.methods['startCampaign(bytes32)'].sendTransaction(
            projectId,
            {
                from: projectsContractOwner
            }
        );
    }

    return {
        assetId,
        projectId
    };
};

/**
 * Create project with ERC20 asset
 * @param {Object} projectsContract ProjectsRegistry contract instance
 * @param {Object} assetsContract AssetsRegistry contract instance
 * @param {string} projectsContractOwner Contracts owner
 * @param {string[]} investors List of investors addresses
 * @param {Object|null|undefined} milestonesOverride
 * @param {number|null|undefined} startDateOverride
 * @param {number"null|undefined} stopDateOverride
 * @param {Boolean} notStartedCampaign
 * @returns {Promise<Object>} Asset and project Ids {assetId,projectId}
 */
module.exports.createProjectWithErc20Asset = async (
    projectsContract, 
    assetsContract,
    projectsContractOwner, 
    investors = [],
    milestonesOverride,
    startDateOverride,
    stopDateOverride,
    notStartedCampaign = false
) => {
    let assetId;
    let projectId;

    const erc20token = await Erc20Token.new('Test Coin', 'TEST', 18, web3.utils.toBN(web3.utils.toWei('1000000000', 'ether')), {
        from: projectsContractOwner
    });

    // send tokens to investors
    await Promise.all(investors.map(investor => erc20token.methods['transfer(address,uint256)'].sendTransaction(
        investor,
        web3.utils.toBN(web3.utils.toWei('100', 'ether')),
        {
            from: projectsContractOwner
        }
    )));
    
    await assetsContract.methods['pause()'].sendTransaction({
        from: projectsContractOwner
    });
    const result2 = await assetsContract.methods['addAsset(uint8,address,uint256)'].sendTransaction(
        AssetsType.Erc20, 
        erc20token.address, 
        web3.utils.toBN('5521'),
        {
            from: projectsContractOwner
        }
    );
    const events2 = result2.logs.filter(l => l.event === 'AssetAdded');
    assetId = events2[0].args.id;
    await assetsContract.methods['unpause()'].sendTransaction({
        from: projectsContractOwner
    });

    const milestones = milestonesOverride || [
        {
            duration: web3.utils.toBN('100'),
            budget: web3.utils.toBN(web3.utils.toWei('30', 'ether'))
        },
        {
            duration: web3.utils.toBN('150'),
            budget: web3.utils.toBN(web3.utils.toWei('50', 'ether'))
        },
        {
            duration: web3.utils.toBN('200'),
            budget: web3.utils.toBN(web3.utils.toWei('60', 'ether'))
        }
    ];
    const startDate = startDateOverride || Math.round(Date.now()/1000) + 10;//now + 10 sec
    const stopDate = stopDateOverride || startDate + (60*60*24*60);

    const result4 = await addProject(
        projectsContract,
        projectsContractOwner,
        'querty',
        UrlType.Ipfs,
        assetId,
        web3.utils.toWei('170', 'ether'),
        startDate,
        stopDate,
        milestones
    );
    
    const events4 = result4.logs.filter(l => l.event === 'ProjectAdded');
    projectId = events4[0].args.id;

    if (!notStartedCampaign) {
        await projectsContract.methods['startCampaign(bytes32)'].sendTransaction(
            projectId,
            {
                from: projectsContractOwner
            }
        );
    }

    return {
        token: erc20token,
        assetId,
        projectId
    };
};

/**
 * Make an investment to the project
 * @param {Object} projectsContract ProjectsRegistry contract instance
 * @param {Object} assetsContract AssetsRegistry contract instance
 * @param {string} projectId Project Id
 * @param {string} valueStr Value represented by string
 * @param {string} assetOwner Address of an asset owner
 * @returns {Promise}
 */
const makeInvestment = async (
    projectsContract, 
    assetsContract,
    projectId,
    valueStr,
    assetOwner
) => {
    const project = await projectsContract.methods['getProject(bytes32)'].call(projectId);
    const asset = await assetsContract.methods['getAsset(bytes32)'].call(project.assetId);    
    const value = web3.utils.toBN(web3.utils.toWei(valueStr, 'ether'));

    if (asset.assetType.toNumber() === AssetsType.Native) {

        const result1 = await projectsContract.methods['invest(bytes32,uint256)'].sendTransaction(
            projectId,
            value,
            {
                from: assetOwner,
                value
            }
        );
        const events1 = result1.logs.filter(l => l.event === 'Investment');
        (events1.length).should.equal(1);
        (events1[0].args.id).should.equal(projectId);
        (events1[0].args.investor).should.equal(assetOwner);
        (events1[0].args.value).should.eq.BN(value);
    } else {

        await (await Erc20Token.at(asset.assetAddress)).methods['approve(address,uint256)'].sendTransaction(
            projectsContract.address, 
            value, 
            {
                from: assetOwner
            }
        );

        const result2 = await projectsContract.methods['invest(bytes32,uint256)'].sendTransaction(
            projectId,
            value,
            {
                from: assetOwner
            }
        );

        const events2 = result2.logs.filter(l => l.event === 'Investment');
        (events2.length).should.equal(1);
        (events2[0].args.id).should.equal(projectId);
        (events2[0].args.investor).should.equal(assetOwner);
        (events2[0].args.value).should.eq.BN(value);
    }
};
module.exports.makeInvestment = makeInvestment;

/**
 * Make a bunch of investments to the project
 * @param {Object} projectsContract ProjectsRegistry contract instance
 * @param {Object} assetsContract AssetsRegistry contract instance
 * @param {string} projectId Project Id
 * @param {string} projectOwner Project owner address
 * @param {Array<Object>} investments List of investments {investor:{string}, value:{string|Object<BN>}}
 * @param {string} assetOwner Address of an asset owner
 * @returns {Promise}
 */
const doInvestments = async (
    projectsContract,
    assetsContract,
    projectId,
    projectOwner,
    investments, 
    startProject = false
) => {

    await Promise.all(investments.map(investment => makeInvestment(
        projectsContract, 
        assetsContract, 
        projectId, 
        web3.utils.isBN(investment.value) ? investment.value : web3.utils.toBN(investment.value), 
        investment.investor
    )));

    if (startProject) {
        await projectsContract.methods['startProject(bytes32)'].sendTransaction(projectId, {
            from: projectOwner
        });
    }
};
module.exports.doInvestments = doInvestments;