const { isqrt } = require('./bnmath');

/**
 * Calculating an amount of funds available to withdraw 
 * @param {Object|string} projectBalance Current project balance 
 * @param {Object|string} withdrawnBalance Already withdrawn balance
 * @param {Array<Object>} milestones Milestones config
 * @param {Object|number} currentMilestone Current milestone number
 * @param {Object|string} serviceFeeRate Service fee rate
 * @returns {Object} Amount of funds available to withdraw
 */
const calculateWithdraw = (
    projectBalance,
    withdrawnBalance,
    milestones,
    currentMilestone,
    serviceFeeRate
) => {
    projectBalance = web3.utils.isBN(projectBalance) ? projectBalance : web3.utils.toBN(projectBalance);
    withdrawnBalance = web3.utils.isBN(withdrawnBalance) ? withdrawnBalance : web3.utils.toBN(withdrawnBalance);
    currentMilestone = web3.utils.isBN(currentMilestone) ? currentMilestone : web3.utils.toBN(currentMilestone);
    serviceFeeRate = web3.utils.isBN(serviceFeeRate) ? serviceFeeRate : web3.utils.toBN(serviceFeeRate);
    let milestonesBalance = web3.utils.toBN('0');
    let milestonesCount = web3.utils.toBN(milestones.length);

    // Calculate actual balance for milestones 
    for (let i = currentMilestone; i < milestones.length; i++) {
        milestonesBalance = milestonesBalance.add(milestones[i].budget);
    }

    let addFunds = projectBalance
        .sub(milestonesBalance)
        .div(milestonesCount.sub(currentMilestone));
    
    let allowedFunds = milestones[currentMilestone].budget
        .add(addFunds)
        .sub(withdrawnBalance);
    
    let fee = allowedFunds.mul(serviceFeeRate).div(web3.utils.toBN(10000));

    return {
        value: allowedFunds.sub(fee),
        fee
    };
};
module.exports.calculateWithdraw = calculateWithdraw;

/**
 * Calculating investors related value to the withdrawn funds
 * @param {Object|string} softCap Softcap value 
 * @param {Object|string} investorBalance Current project balance of the investor 
 * @param {Object|string} withdrawnValue Withdrawn value 
 * @returns {Object} Tokens value as BN
 */
const calculateRelatedValue = (
    softCap,
    investorBalance,
    withdrawnValue
) => {
    softCap = web3.utils.isBN(softCap) ? softCap : web3.utils.toBN(softCap);
    investorBalance = web3.utils.isBN(investorBalance) ? investorBalance : web3.utils.toBN(investorBalance);
    withdrawnValue = web3.utils.isBN(withdrawnValue) ? withdrawnValue : web3.utils.toBN(withdrawnValue);

    return investorBalance
        .mul(withdrawnValue.mul(web3.utils.toBN(100)).div(softCap))
        .div(web3.utils.toBN(100));
};
module.exports.calculateRelatedValue = calculateRelatedValue;

/**
 * Calculating a value of the investment tokens 
 * @param {Object|string} investmentValueStr Investment value 
 * @param {Object|string} softCapStr Softcap value 
 * @param {Object|string} nativeFactorStr Native factor string
 * @returns {Object} Tokens value as BN
 */
const calculateInvestmentTokens = (
    investmentValueStr,
    softCapStr,
    nativeFactorStr
) => {
    investmentValueStr = web3.utils.isBN(investmentValueStr) ? investmentValueStr : web3.utils.toBN(web3.utils.toWei(investmentValueStr, 'ether'));
    nativeFactorStr = web3.utils.isBN(nativeFactorStr) ? nativeFactorStr : web3.utils.toBN(nativeFactorStr);
    softCapStr = web3.utils.isBN(softCapStr) ? softCapStr : web3.utils.toBN(web3.utils.toWei(softCapStr, 'ether'));

    let tokensValue = investmentValueStr
        .mul(web3.utils.toBN(100))
        .mul(nativeFactorStr)
        .div(softCapStr);

    if (tokensValue.toString() === '0') {
        tokensValue = web3.utils.toBN('1');
    }

    return tokensValue;
};
module.exports.calculateInvestmentTokens = calculateInvestmentTokens;

/**
 * Assert eraned investment tokens
 * @param {Object} projectsContract ProjectsRegitry contract instance
 * @param {Object} assetsContract AssetsRegistry contract instance
 * @param {Object} tokenContract Investment token contract instance
 * @param {string} projectId Project Id
 * @param {Object} withdrawnValue Withdrawn value (BN)
 * @param {string} investor Address of the investor
 * @param {Object} initialBalance Initial token balance (before investment)
 */
module.exports.assertInvestmentTokens = async (
    projectsContract,
    assetsContract,
    tokenContract,
    projectId,
    withdrawnValue,
    investor,
    initialBalance
) => {
    const project = await projectsContract.methods['getProject(bytes32)'].call(projectId);
    const asset = await assetsContract.methods['getAsset(bytes32)'].call(project.assetId);
    const investorBalance = await projectsContract.methods['investorBalance(bytes32,address)'].call(projectId, investor);
    const balance = await tokenContract.methods['balanceOf(address)'].call(investor);
    const relatedValue = calculateRelatedValue(
        project.softCap,
        investorBalance,
        withdrawnValue
    );
    const tokensValue = calculateInvestmentTokens(
        relatedValue, 
        project.softCap,
        asset.nativeFactor
    );
    (balance).should.eq.BN(tokensValue.sub(initialBalance));
};

/**
 * Assert tokens burn during ragequit
 * @param {Object} projectsContract ProjectsRegitry contract instance
 * @param {Object} assetsContract AssetsRegistry contract instance
 * @param {Object} tokenContract Investment token contract instance
 * @param {string} projectId Project Id
 * @param {Object} quitValue Ragequit value (BN)
 * @param {string} investor Address of the investor
 * @param {Object} initialBalance Initial token balance (before investment)
 */
module.exports.assertInvestmentTokensBurn = async (
    projectsContract,
    assetsContract,
    tokenContract,
    projectId,
    quitValue,
    investor,
    initialBalance
) => {
    const project = await projectsContract.methods['getProject(bytes32)'].call(projectId);
    const asset = await assetsContract.methods['getAsset(bytes32)'].call(project.assetId);
    const balance = initialBalance.sub((await tokenContract.methods['balanceOf(address)'].call(investor)));
    (balance).should.eq.BN(calculateInvestmentTokens(
        quitValue, 
        project.softCap,
        asset.nativeFactor
    ));
};

/**
 * Calculating a voting threshold
 * @param {Object|string} projectBalance Current project balance 
 * @param {Object|string} withdrawnBalance Already withdrawn balance
 * @param {Array<Object>} milestones Milestones config
 * @param {Object|number} currentMilestone Current milestone number
 * @param {Object|string} serviceFeeRate Service fee rate
 * @param {number} softCap Project sofcap
 * @param {number} nativeFactor Project asset native factor
 * @returns {Object} Amount of funds available to withdraw
 */
module.exports.calculateVotingThreshold = (
    projectBalance, 
    withdrawnBalance,
    milestones,
    currentMilestone,
    serviceFeeRate,
    softCap,
    nativeFactor
) => {
    projectBalance = web3.utils.isBN(projectBalance) ? projectBalance : web3.utils.toBN(projectBalance);
    withdrawnBalance = web3.utils.isBN(withdrawnBalance) ? withdrawnBalance : web3.utils.toBN(withdrawnBalance);
    currentMilestone = web3.utils.isBN(currentMilestone) ? currentMilestone : web3.utils.toBN(currentMilestone);
    serviceFeeRate = web3.utils.isBN(serviceFeeRate) ? serviceFeeRate : web3.utils.toBN(serviceFeeRate);
    
    const milestoneFunds = calculateWithdraw(
        projectBalance,
        withdrawnBalance,
        milestones,
        currentMilestone,
        serviceFeeRate
    );

    const baseTokens = calculateInvestmentTokens(
        milestoneFunds.value,
        softCap,
        nativeFactor
    );

    return isqrt(baseTokens
        .div(web3.utils.toBN('2'))
        .add(web3.utils.toBN('1')));
};