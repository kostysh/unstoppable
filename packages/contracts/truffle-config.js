const HDWalletProvider = require('truffle-hdwallet-provider');
const NonceTrackerSubprovider = require('web3-provider-engine/subproviders/nonce-tracker');

let pkey;
let apiKey;

try {
    const keys = require('./localkeys.json');
    pkey = keys.pkey;
    apiKey = keys.infuraApiKey;
} catch (e) {
    console.log('Keys not found or localkeys.json file is corrupt');// eslint-disable-line no-console
}

module.exports = {

    plugins: [
        'truffle-security',
        'solidity-coverage'
    ],

    contracts_build_directory: './artifacts',

    networks: {
        ganache: {
            host: '127.0.0.1',
            port: 8545,
            network_id: '*',
            websockets: true,
            gas: 8000000// Like in Ropsten network
        },
        infura_ropsten: {
            provider: _ => {// eslint-disable-line no-unused-vars
                const provider = new HDWalletProvider(pkey, `https://ropsten.infura.io/v3/${apiKey}`);
                const nonceTracker = new NonceTrackerSubprovider();
                provider.engine.addProvider(nonceTracker);
                return provider;
            },
            network_id: 3,
            gas: 5500000,
            confirmations: 2,
            timeoutBlocks: 200,
            skipDryRun: true
        },
    },

    mocha: {
        enableTimeouts: false
        // timeout: 100000
    },

    compilers: {
        solc: {
            version: '0.5.11',
            settings: {
                optimizer: {
                    enabled: true,
                    runs: 200
                }
            }
        }
    }
};
