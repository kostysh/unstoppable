# Smart contracts

## Initial setup

```bash
npm i
```

## Build contracts artifacts

```bash
npm run build
```
Artifacts will be placed in `./artifacts` folder.

## Tests

```bash
npm run test
npm run test ./<path_to_test_file>.js
```

## Linting

```bash
npm run lint
```

## Security verification

```bash
MYTHX_ETH_ADDRESS=<registered_eth_address> MYTHX_PASSWORD=<account_password> npm run verify
```
MythX registration instructions are published [here](https://docs.mythx.io/en/latest/getting-started/index.html)
